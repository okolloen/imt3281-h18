package no.ntnu.imt3281.exceptions;

public class UsingAssert {
	public static void main(String[] args) {
		int a = 5;
		
		assert (a>10) :"a er feil "+a;	// Må kjøres med -ea for å gjøre en forskjell (Enable Assert)
		
		System.out.println(a);
	}
}
