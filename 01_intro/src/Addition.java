import java.util.Scanner;

public class Addition {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Skriv inn første tallet:");
		int number1 = input.nextInt();
		
		System.out.println("Skriv inn andre tallet");
		int number2 = input.nextInt();
		
		int sum = number1+number2;
		
		System.out.printf("Summen av %d og %d er %d\n", number1, number2, sum);
		input.close();
	}
}
