package no.ntnu.imt3281.inheritance;

public class B extends A {
	public B() {
		super();
		System.out.println("Constructor of B was called");
	}
}
