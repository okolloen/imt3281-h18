package no.ntnu.imt3281.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DivideByZeroWithExceptionHandling {
	
	public static int quotient(int numerator, int denominator) 
		throws ArithmeticException {
		return numerator/denominator;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		boolean continueLoop = true;
		
		do {
			try {
				System.out.println("Skriv inn teller: ");
				int numerator = scanner.nextInt();
				System.out.println("Skriv inn nevner: ");
				int denominator = scanner.nextInt();
				
				int result = quotient(numerator, denominator);
				System.out.printf("\nResult: %d/%d=%d", numerator, denominator, result);
				continueLoop = false;	// Success, stop the loop
			} catch (InputMismatchException ime) {
				System.err.printf("\nException: %s", ime);
				scanner.nextLine();
				System.out.println("Du må skrive inn heltall, prøv igjen.");
			} catch (ArithmeticException ae) {
				System.err.printf("\nException: %s", ae);
				System.out.println("0 er ikke en gyldig nevner, prøv igjen.");
			}
		} while (continueLoop);	// Loop until success

	}

}
