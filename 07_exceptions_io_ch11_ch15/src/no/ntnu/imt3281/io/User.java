package no.ntnu.imt3281.io;

public class User {
	int id;
	String firstName, lastName, ip;
	
	public User() {
		id=0; 
		firstName = "";
		lastName = "";
		ip = "";
	}
	
	/**
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param ip
	 */
	public User(int id, String firstName, String lastName, String ip) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.ip = ip;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
}
