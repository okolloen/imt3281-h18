package no.ntnu.imt3281.listView;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import no.ntnu.imt3281.listView.ListViewControllerLive.Browser;

public class ImageTextCell1 extends ListCell<Browser> {

	private VBox vbox = new VBox(8.0);
	private ImageView thumb = new ImageView();
	private Label label = new Label();
	
	public ImageTextCell1() {
		vbox.setAlignment(Pos.CENTER);
		
		thumb.setPreserveRatio(true);
		thumb.setFitHeight(100);
		vbox.getChildren().add(thumb);
		
		label.setWrapText(true);
		label.setTextAlignment(TextAlignment.CENTER);
		vbox.getChildren().add(label);
		
		setPrefWidth(USE_PREF_SIZE);
	}
	
	@Override
	protected void updateItem(ListViewControllerLive.Browser item,
			boolean empty) {
		super.updateItem(item, empty);
		if (empty||item==null) {
			setGraphic(null);
		} else {
			thumb.setImage(new Image(
				getClass().getResourceAsStream(item.getImg())));
			label.setText(item.getName());
			setGraphic(vbox);
		}
	}
}
