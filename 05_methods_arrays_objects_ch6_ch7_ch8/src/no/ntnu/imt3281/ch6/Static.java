package no.ntnu.imt3281.ch6;

import java.util.Scanner;

public class Static {
	static String formatString = "Det største taller er %.4f";
	
	public static void main(String[] args) {
		System.out.println("Skriv inn tre desimaltall, skill de med mellomrom:");
		Scanner input = new Scanner(System.in);
		double number1 = input.nextDouble();
		double number2 = input.nextDouble();
		double number3 = input.nextDouble();
		double result = maximum (number1, number2, number3);
		
		System.out.printf(formatString, result);
		input.close();
	}
	
	static double maximum (double x, double y, double z) {
		return (x>y)
				?(x>z)?x:z
				:(y>z)?y:z;
	}

}
