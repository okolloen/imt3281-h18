package streams;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Streams {
	static void intStreams() {
		Integer n[] = { 4, 1, 7, 3, 8, 2, 5, 6, 9 };
		
		String str = Arrays.stream(n)	// Need stream of objects - not int
				.filter(x -> x%2==0)
				.map(x -> x*3)
				.sorted()
				.collect(Collectors.toList()).toString();
		System.out.printf("Tall fra 1-10, finn partall, multipliser med 3, sorterer : %s\n", 
				str);
	}
	
	static void StringStreams() {
		String strings[] = { "Red", "orange", "Yellow", "green", "Blue", "indigo", "Violet" };
		
		System.out.printf("Originale strenger: %s\n", Arrays.asList(strings));
		
		System.out.printf("Tekster med store bokstaver :%s\n", Arrays.stream(strings)
				.map(String::toUpperCase)
				.collect(Collectors.toList()));
		
		System.out.printf("Mindre enn 'n', sortert stigende :%s\n", Arrays.stream(strings)
				.filter(s -> s.compareToIgnoreCase("n")<0)
				.sorted(String.CASE_INSENSITIVE_ORDER)
				.collect(Collectors.toList()));
	
		System.out.printf("Mindre enn 'n', sortert avtagende :%s\n", Arrays.stream(strings)
				.filter(s -> s.compareToIgnoreCase("n")<0)
				.sorted(String.CASE_INSENSITIVE_ORDER.reversed())
				.collect(Collectors.toList()));
	}
	
	public static void main(String[] args) {
		intStreams();
		StringStreams();
	}
}
