package no.ntnu.imt3281.exceptions;

import java.util.Scanner;

public class TryWithResource {
	public static void main(String[] args) {
		try (Scanner input = new Scanner(System.in)) {
			System.out.println("Skriv inn en tekst");
			input.nextLine();
		} // catch er valgfritt
		finally {
			System.out.println("input er frigjort");
		}
		
		
		MustBeReleased ouch = new MustBeReleased();
		try (ouch) {		// Fungerer i java 9+
			System.out.println("Gjøre noe fornuftig med 'ouch'");
		}
		System.out.println("End of main");
	}
}

class MustBeReleased implements AutoCloseable{
	MustBeReleased() {
		System.out.println("I will eat all your memory and CPU");
	}

	@Override
	public void close() {
		System.out.println("OK, I'm closed and won't eat all your memory and CPU");
	}
}
