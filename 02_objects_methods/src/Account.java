public class Account {
	private String name;
	private double balance;

	// Autogenerer getters/setters, m/kommentarer

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public void deposit (double amount) {
		if (amount > 0) {
			balance += amount;
		}
	}
	
	public double getBalance () {
		return balance;
	}
	
	// Autogenerer constructor
	/**
	 * @param name
	 */
	public Account(String name, double balance) {
		this.name = name;
		if (balance >0) {
			this.balance = balance;
		}
	}
}