package no.ntnu.imt3281.exceptions;

import java.util.Scanner;

public class DivideByZeroNoExceptionHandling {

	public static int quotient(int numerator, int denominator) {
		return numerator/denominator;
	}
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Skriv inn teller");
		int numerator = scanner.nextInt();
		
		System.out.println("Skriv inn nevner");
		int denominator = scanner.nextInt();
		
		int result = quotient(numerator, denominator);
		System.out.printf("Resultat %d/%d = %d", numerator, denominator, result);
		scanner.close();
	}

}
