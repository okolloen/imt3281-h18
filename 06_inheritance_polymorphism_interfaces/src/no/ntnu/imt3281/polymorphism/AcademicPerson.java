package no.ntnu.imt3281.polymorphism;

public abstract class AcademicPerson extends Person {
	private String school;
	
	public AcademicPerson (String givenName, String sureName, String school) {
		super (givenName, sureName);
		this.school = school;
	}

	public String getSchool() {
		return school;
	}
}
