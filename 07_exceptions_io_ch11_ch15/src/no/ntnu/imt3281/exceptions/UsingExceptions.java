package no.ntnu.imt3281.exceptions;

public class UsingExceptions {
	public static void throwException() throws Exception {
		try {
			System.out.println("Method 'throwException'");
			throw new Exception("Error in method");
		} catch (Exception e) {
			System.out.println("Handling exception in 'throwException'");
			throw e;
		} finally {
			System.out.println("finally excecuted in 'throwException'");
		}
	}
	
	public static void main(String[] args) {
		try {
			throwException();
		} catch (Exception e) {
			System.out.printf("Exception handled in main: %s\n", e);
		}
		
		doesNotThrowException();
	}

	private static void doesNotThrowException() {
		try {
			System.out.println("Nothing wrong in 'doesNotThrowException'");
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			System.out.println("finally executed in 'doesNotThrowException'");
		}
	}
}
