package losningForrigeUke;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class CarController {
	Car cars[] = new Car[5];
	int numCars = 0;
	
    @FXML
    private TextField model;

    @FXML
    private TextField year;

    @FXML
    private TextField price;

    @FXML
    private TextArea utskrift;

    @FXML
    void createCar(ActionEvent event) {
    	if (numCars<cars.length-1) {
    		cars[numCars] = new Car(
    				model.getText(),
    				year.getText(),
    				Double.parseDouble(price.getText()));
        	utskrift.setText(utskrift.getText()+
        			cars[numCars]+"\n");
        	numCars++;
    	}
    }

    @FXML
    void giveDiscount(ActionEvent event) {

    }

    @FXML
    void writeCars(ActionEvent event) {

    }
}