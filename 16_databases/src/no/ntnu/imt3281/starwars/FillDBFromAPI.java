package no.ntnu.imt3281.starwars;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javafx.application.Application;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class FillDBFromAPI extends Application {
	@FXML private Button startstop;
    @FXML private TextField vehiclesFound;
    @FXML private TextField peopleFound;
    @FXML private TextField planetsFound;
    @FXML private TextField vehiclesRead;
    @FXML private TextField filmsFound;
    @FXML private TextField speciesRead;
    @FXML private TextField filmsRead;
    @FXML private TextField peopleRead;
    @FXML private TextField starshipsFound;
    @FXML private TextField starshipsRead;
    @FXML private TextField planetsRead;
    @FXML private TextField speciesFound;

	private final static int READFROMSWAPITHREADS = 5; 

	private final String[] resourceTypes = { "films", "people", "planets", "species", "starships", "vehicles" };
	private final ArrayBlockingQueue<String> itemsToGet = new ArrayBlockingQueue<>(600);
	private final List<String> itemsFound = new ArrayList<>();
	private boolean stopReading = false;
	private final HashMap<String, SimpleIntegerProperty> foundCounters = new HashMap<>();
	private final HashMap<String, SimpleIntegerProperty> readCounters = new HashMap<>();
	private Connection dbCon = null; 
	
	@FXML
	public void initialize() {
		initializeCounters();
		filmsFound.textProperty().bind(foundCounters.get("films").asString());
		peopleFound.textProperty().bind(foundCounters.get("people").asString());
		planetsFound.textProperty().bind(foundCounters.get("planets").asString());
		speciesFound.textProperty().bind(foundCounters.get("species").asString());
		starshipsFound.textProperty().bind(foundCounters.get("starships").asString());
		vehiclesFound.textProperty().bind(foundCounters.get("vehicles").asString());
		filmsRead.textProperty().bind(readCounters.get("films").asString());
		peopleRead.textProperty().bind(readCounters.get("people").asString());
		planetsRead.textProperty().bind(readCounters.get("planets").asString());
		speciesRead.textProperty().bind(readCounters.get("species").asString());
		starshipsRead.textProperty().bind(readCounters.get("starships").asString());
		vehiclesRead.textProperty().bind(readCounters.get("vehicles").asString());
		startDB();
		startThreads();
	}
	
	/**
	 * Connects to the database and makes sure the table for storing data exists.
	 */
	private void startDB() {
		try {
			dbCon = DriverManager.getConnection("jdbc:derby:starWarsDB;create=true");
			initializeDB();
		} catch (SQLException e) {
			System.err.printf("Database error : %s", e.getMessage());
			System.exit(0);
		}
	}

	/**
	 * Creates table and indexes. 
	 * 
	 * @throws SQLException if any problems besides table exists occurs an exception will be thrown. 
	 */
	private void initializeDB() throws SQLException {
		try {
			String sql = "CREATE TABLE SWAPIlocal (id int not null, "+
												"type varchar(32) not null, "+
												"swapiURL varchar(255) not null, "+
												"jsonPayload varchar(32672) not null, "+
												"UNIQUE (id, type))";
			Statement stmnt = dbCon.createStatement();
			stmnt.execute(sql);
			sql = "CREATE INDEX id ON SWAPIlocal (id)";
			stmnt.execute(sql);
			sql = "CREATE INDEX type ON SWAPIlocal (type)";
			stmnt.execute(sql);
			sql = "CREATE UNIQUE INDEX swapiURL ON SWAPIlocal (swapiURL)";
			stmnt.execute(sql);
		} catch (SQLException e) {
			if (!e.getSQLState().equals("X0Y32")) {	/// X0Y32 means table exists
				throw (e);
			}
		}
	}

	/**
	 * Starts threads used to read from SWAPI and set up the properties used
	 * to keep count on found and read resources.
	 */
	private void startThreads() {
		ExecutorService executor = Executors.newCachedThreadPool();
		for (int i=0; i<READFROMSWAPITHREADS; i++) {
			executor.execute(()->readFromSWAPI());
		}
	}

	/**
	 * Creates and initialize to 0 SimpleIntegerProperty objects for all
	 * counters.
	 */
	private void initializeCounters() {
		Arrays.asList(resourceTypes).forEach(type->{
			foundCounters.put(type, new SimpleIntegerProperty());
			foundCounters.get(type).set(0);
			readCounters.put(type, new SimpleIntegerProperty());
			readCounters.get(type).set(0);
		});
	}
	
	/**
	 * Method runs in separate threads and reads from SWAPI.
	 * Gets URL's to read from the buffer itemsToGet and keeps
	 * running until stopReading goes true.
	 * 
	 * When an item has been read it is inserted into the database,
	 * then parsed and any new items (not yet found items) will be
	 * added to the queue.
	 */
	private void readFromSWAPI() {
		while (!stopReading) {
			String url=null;
			try {
				url = itemsToGet.poll(1, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			if (url!=null) {
				try {
					System.out.println("Reading: "+url);
					URL u = new URL(url+"?format=json");
					BufferedReader br = new BufferedReader(new InputStreamReader(u.openStream()));
					StringBuffer sb = new StringBuffer();
					String tmp = null;
					while ((tmp = br.readLine())!=null) {
						sb.append(tmp);
					}
					br.close();
					System.out.println("Read: "+url);
					insertIntoDB(url, sb.toString());
					readCounters.get(typeFromURL(url)).set(readCounters.get(typeFromURL(url)).get()+1);
					StarWarsEntry entry = new StarWarsEntry(sb.toString());
					findNewItems(entry);
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}

	/**
	 * Insert a new entry into the database.
	 * 
	 * @param url the source for this entry, contains type and id
	 * @param jsonPayload the contents read from the source url
	 */
	private void insertIntoDB(String url, String jsonPayload) {
		String sql = "INSERT INTO SWAPIlocal (id, type, SWAPIurl, jsonPayload) VALUES (?, ?, ?, ?)";
		try {
			PreparedStatement stmnt = dbCon.prepareStatement(sql);
			stmnt.setInt(1, idFromURL(url));
			stmnt.setString(2, typeFromURL(url));
			stmnt.setString(3, url);
			stmnt.setString(4, jsonPayload);
			stmnt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Called from the startStop button.
	 * 
	 * @param e not used
	 */
	@FXML
	void startStop(ActionEvent e) {
		if (startstop.getText().equals("Start")) {
			startstop.setText("Stop");
			start();
		} else {
			stopReading = true;
			startstop.setDisable(true);
		}
	}
	
	/**
	 * Initiates reading from SWAPI, will add the first movie to 
	 * the queue to be read and thus seed the process.
	 */
	public void start() {
		stopReading = false;
		try {
			itemsFound.add("https://swapi.co/api/films/1/");	// Should not read this again
			itemsToGet.put("https://swapi.co/api/films/1/");	// Read this to get a starting point
			foundCounters.get("films").set(foundCounters.get("films").get()+1);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * When a resource has been read from SWAPI this method is called to find
	 * any new resources not yet discovered.
	 * 
	 * @param entry an object of the StarWarsEntry class, contains lists to all connected resources.
	 */
	private synchronized void findNewItems(StarWarsEntry entry) {
		Arrays.asList(resourceTypes).forEach(list->{	
			if (entry.getList(list)!=null) {
				entry.getList(list).forEach(item-> {
					if (!itemsFound.contains(item)) {
						itemsFound.add(item);
						itemsToGet.add(item);
						foundCounters.get(typeFromURL(item)).set(foundCounters.get(typeFromURL(item)).get()+1);
					}
				});
			}
		});
	}

	/**
	 * Will get the type of resource for this url (films, people, vehicles etc.)

	 * @param url the resource to find the type for
	 * @return a string representing the type for this resource
	 */
	private String typeFromURL(String url) {
		String type = url.substring(0, url.lastIndexOf('/', url.length()-2));	// Remove /[0-9]+/
		return type.substring(type.lastIndexOf('/', type.length())+1);
	}
	
	/**
	 * Will get the id of the resource for this url
	 * 
	 * @param url the resource to find the id for
	 * @return an int with the id of this resource
	 */
	private int idFromURL(String url) {
		return Integer.parseInt(url.substring(url.lastIndexOf('/', url.length()-2)+1, url.length()-1));
	}

	/**
	 * Loads the fxml file representing the GUI for this application and sets up the main window.
	 */
	@Override
	public void start(Stage primaryStage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("FillDBFromAPI.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Read from SWAPI and copy to local DB");
		primaryStage.show();
	}
	
	/**
	 * Starts the program.
	 * 
	 * @param args not used
	 */
	public static void main(String[] args) {
		launch(args);
		System.exit(0);	// Close the program when the GUI closes
	}
}