/**
 * 
 */
package no.ntnu.imt3281.generic_data_structures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author oeivindk
 *
 */
public class MyIterator <T> implements Iterator<T> {

	List<T> values = new ArrayList<>();
	
	@SafeVarargs
	public MyIterator(T ...args) {		// Why T ...args instead of T[] args???
		for (T value : args) {
			values.add(value);
		}
	}
	
	@Override
	public boolean hasNext() {
		return values.size()>0;
	}

	@Override
	public T next() {
		if (values.size()>0) {
			return values.remove(0);
		}
		throw new NoSuchElementException("No more items in iterator");
	}
}
