package no.ntnu.imt3281.three_d_shapes;

import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Sphere;

public class ThreeDimensionsController {
    @FXML private Box box;
    @FXML private Sphere sphere;
    @FXML private Slider sliderTop;
    @FXML private Slider sliderBottom;
    @FXML private Slider sliderLeft;
    @FXML private Slider sliderRight;

    @FXML
    public void initialize() {
    	PhongMaterial boxMaterial = new PhongMaterial();
    	boxMaterial.setDiffuseMap(new Image(getClass().getResourceAsStream("spitfire.jpg")));
    	box.setMaterial(boxMaterial);
    	box.rotateProperty().bind(sliderLeft.valueProperty());
    	box.scaleXProperty().bind(sliderTop.valueProperty());
    	PhongMaterial sphereMaterial = new PhongMaterial();
    	sphereMaterial.setDiffuseMap(new Image(getClass().getResourceAsStream("home.jpg")));
    	sphere.setMaterial(sphereMaterial);
    	sphere.rotateProperty().bind(sliderRight.valueProperty());
    	sphere.scaleXProperty().bind(sliderBottom.valueProperty());
    	sphere.scaleYProperty().bind(sliderBottom.valueProperty());
    	sphere.scaleZProperty().bind(sliderBottom.valueProperty());
    	
    }
}
