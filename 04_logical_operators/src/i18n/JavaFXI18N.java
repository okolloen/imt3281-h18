package i18n;

import java.io.IOException;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class JavaFXI18N extends Application {

	@Override
	public void start(Stage primaryStage) throws IOException {
        ResourceBundle bundle = ResourceBundle.getBundle("i18n.MessagesBundle");
		VBox pane = (VBox) FXMLLoader.load(this.getClass().getResource("i18n.fxml"), bundle);
        primaryStage.setScene(new Scene(pane));
        primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
