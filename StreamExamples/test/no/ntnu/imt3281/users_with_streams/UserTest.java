package no.ntnu.imt3281.users_with_streams;

import static org.junit.Assert.*;

import org.junit.Test;

public class UserTest {

	@Test
	public void testConstructor() {
		User u = new User("1,Ola,Nordmann,ola@nordmann.no,Male,127.0.0.1");
		assertEquals(1, u.getId());
		assertEquals("Ola", u.getFirst());
		assertEquals("Nordmann", u.getLast());
		assertEquals("ola@nordmann.no", u.getEmail());
		assertEquals("Male", u.getGender());
		assertEquals("127.0.0.1", u.getIp());
	}

}
