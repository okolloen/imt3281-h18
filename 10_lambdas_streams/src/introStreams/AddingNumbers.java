package introStreams;

import java.util.stream.IntStream;

public class AddingNumbers {
	public static void main(String[] args) {
		int numbers[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		
		int sum = 0;
		for (int i=0; i<numbers.length; i++) {
			sum += numbers[i];
		}
		System.out.printf("The sum of 1+2+3+4+5+6+7+8+9 is : %d\n", sum);
		
		sum = 0;
		for (int number : numbers) {
			sum += number;
		}
		System.out.printf("The sum of 1+2+3+4+5+6+7+8+9 is : %d\n", sum);
		
		sum = IntStream.of(numbers).sum();
		System.out.printf("The sum of 1+2+3+4+5+6+7+8+9 is : %d\n", sum);		
	}
}
