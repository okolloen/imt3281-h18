package no.ntnu.imt3281.math;

import java.math.BigInteger;

/**
 * 0! = 1
 * 1! = 1
 * n! = n*(n-1)!
 * 
 * @author oeivindk
 *
 */
public class FactorialCalculator {
	public static long factorial(long number) {
		if (number<=1) {	// 0!=1 og 1!=1
			return 1;
		} else {
			return number*factorial(number-1);
		}
	}
	
	
	public static long nonRecursiveFactorial(int number) {
		if (number==0) {
			return 1;
		}
		long factorial = 1;
		for (int i=1; i<= number; i++) {
			factorial *= i;
		}
		return factorial;
	}
	
	public static BigInteger factorial (BigInteger number) {
		if (number.compareTo(BigInteger.ONE)<=0) {
			return BigInteger.ONE;
		} else {
			return number.multiply(factorial(number.subtract(BigInteger.ONE)));
		}
	}
	
	public static void main(String[] args) {
		for (int i=0; i<6; i++) {
			System.out.printf("%d! = %d\n" , i, nonRecursiveFactorial(i));
		}
		
		for (int i=0; i<22; i++) {
			System.out.printf("%d! = %d\n" , i, factorial(i));
		}
			
		for (int i=0; i<51; i++) {
			System.out.printf("%d! = %d\n" , i, factorial(BigInteger.valueOf(i)));
		}
	}
}
