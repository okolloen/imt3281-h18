package no.ntnu.imt3281.ch6;

import java.security.SecureRandom;

public class RollDice1 {

	public static void main(String[] args) {
		SecureRandom random = new SecureRandom();
		int frequenzy[] = new int[6];
		for (int roll=0; roll<60000; roll++) {
			frequenzy[random.nextInt(6)]++;
		}
		System.out.println("Face\tfrequenzy");
		for (int face=0; face<6; face++) {
			System.out.printf("%d\t%d\n", face+1, frequenzy[face]);			
		}
	}

}
