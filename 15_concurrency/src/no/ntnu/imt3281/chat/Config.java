package no.ntnu.imt3281.chat;

/**
 * Class contains constants used by the program.
 * Change the servername and or port here to use different 
 * servers.
 * 
 * @author oeivindk
 *
 */
public class Config {
	private Config() {}
	/**
	 * The name of the server, since the server and client
	 * in this example is running in the same program using
	 * localhost makes the most sense.
	 */
	public static final String SERVERNAME = "localhost";
	/**
	 * Run (and connect to) the server on this port.
	 * Change this if something else is using this port.
	 */
	public static final int SERVERPORT = 9050;
}
