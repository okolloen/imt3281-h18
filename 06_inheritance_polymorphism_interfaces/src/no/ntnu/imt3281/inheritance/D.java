package no.ntnu.imt3281.inheritance;

public class D extends C {
	private String name;
	
	public D (String name) {
		this.name = name;
		System.out.println("Constructor of D was called");
	}
	
	@Override
	public String toString() {
		return String.format("I'm %s of class %s", name, getClass().getSimpleName());
	}
}
