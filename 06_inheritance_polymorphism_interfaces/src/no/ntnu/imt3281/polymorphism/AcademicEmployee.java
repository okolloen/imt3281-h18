package no.ntnu.imt3281.polymorphism;

public class AcademicEmployee extends AcademicPerson {
private String title;
	
	public AcademicEmployee(String givenName, String sureName, String school, String title) {
		super(givenName, sureName, school);
		this.title = title;
	}

	
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return String.format("%s %s is a %s at %s", getGivenName(), getSureName(), title, getSchool());
	}
}
