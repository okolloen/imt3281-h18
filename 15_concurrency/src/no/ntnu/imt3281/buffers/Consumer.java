package no.ntnu.imt3281.buffers;

import java.security.SecureRandom;

public class Consumer implements Runnable {
	private static final SecureRandom random = new SecureRandom();
	private final Buffer<Integer> sharedLocation;
	
	public Consumer (Buffer<Integer> sharedLocation) {
		this.sharedLocation = sharedLocation;
	}

	@Override
	public void run() {
		int sum = 0;
		
		for (int count=1; count<=10; count++) {
			try {
				Thread.sleep(random.nextInt(3000));
				sum += sharedLocation.get();
				System.out.printf("\t\t\t\t\t\t%7d%n", sum);
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			} catch (NullPointerException npe) {
				System.out.println("Consumer got NULL");
			}
		}
		System.out.printf("Det er lest verdier med en sum av %d, avslutter lesing%n", sum);
	}
}
