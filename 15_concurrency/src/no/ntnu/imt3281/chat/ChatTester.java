package no.ntnu.imt3281.chat;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class ChatTester {
    @FXML private BorderPane serverLivesHere;
    @FXML private GridPane clientsLiveHere;

    @FXML
    public void initialize() throws IOException {
    	for (int i=0; i<6; i++) {	// Add six clients
    		BorderPane client = FXMLLoader.load(getClass().getResource("Client.fxml"));
    		clientsLiveHere.add(client, i%2, i/2);
    	}
    	BorderPane server = FXMLLoader.load(getClass().getResource("Server.fxml"));
    	serverLivesHere.setCenter(server);
    }
}