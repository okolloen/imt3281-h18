package no.ntnu.imt3281.buffers;

import java.security.SecureRandom;

public class Producer implements Runnable {
	private static final SecureRandom random = new SecureRandom();
	private final Buffer<Integer> sharedLocation;
	
	public Producer (Buffer<Integer> sharedLocation) {
		this.sharedLocation = sharedLocation;
	}

	@Override
	public void run() {
		int sum = 0;
		
		for (int count=1; count<=10; count++) {
			try {
				Thread.sleep(random.nextInt(3000));
				sharedLocation.put(count);
				sum += count;
				System.out.printf("\t\t\t\t\t%2d%n", sum);
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
		System.out.printf("Det er skrevet verdier med en sum av %d, avslutter skriving%n", sum);
	}
}
