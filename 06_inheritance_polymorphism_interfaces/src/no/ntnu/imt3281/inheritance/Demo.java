package no.ntnu.imt3281.inheritance;

public class Demo {
	public static void main(String[] args) {
		A a = new A();
		System.out.println(a);
		System.out.println("================");
		B b = new B();
		System.out.println(b);
		System.out.println("================");
		C c = new C();
		System.out.printf("Methods in %s\n",c);
		for (String s : c.getMethodNames()) {
			System.out.println(s);
		}
		System.out.println("================");
		D d = new D("Snømann");
		System.out.println(d);
		
	}
}
