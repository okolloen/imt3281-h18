package no.ntnu.imt3281.ch6;

import java.security.SecureRandom;

public class RollDice {

	public static void main(String[] args) {
		SecureRandom random = new SecureRandom();
		int frequenzy1 = 0, frequenzy2 = 0, frequenzy3 = 0, frequenzy4 = 0, frequenzy5 = 0, frequenzy6 = 0;
		for (int roll=0; roll<60000; roll++) {
			int face = random.nextInt(6)+1;
			switch (face) {
			case 1:
				frequenzy1++;
				break;
			case 2:
				frequenzy2++;
				break;
			case 3:
				frequenzy3++;
				break;
			case 4:
				frequenzy4++;
				break;
			case 5:
				frequenzy5++;
				break;
			case 6:
				frequenzy6++;
				break;
			}
		}
		System.out.println("Face\tfrequenzy");
		System.out.printf("1\t%d\n2\t%d\n3\t%d\n4\t%d\n5\t%d\n6\t%d\n", frequenzy1, frequenzy2, frequenzy3,frequenzy4, frequenzy5, frequenzy6);
	}
}
