package no.ntnu.imt3281.chat;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("ChatTester.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Chat system tester");
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
		System.exit(0); 	// Shut down application when GUI closes
	}
}
