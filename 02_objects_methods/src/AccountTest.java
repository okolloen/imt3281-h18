

public class AccountTest {
	public static void printAccounts (Account account1, Account account2) {
		System.out.printf("På kontoen til %s står det %.2fkr\n", account1.getName(), account1.getBalance());
		System.out.printf("På kontoen til %s står det %.2fkr\n\n", account2.getName(), account2.getBalance());
	}
	
	public static void main(String[] args) {		
		Account account1 = new Account("Kari", 0);
		Account account2 = new Account("Ola", 10);
		
		printAccounts(account1, account2);
		
		System.out.printf("Setter inn 100kr på kontoen til %s\n", account1.getName());
		account1.deposit(100);
		
		printAccounts(account1, account2);
		
		System.out.printf("Setter inn 30 øre på kontoen til %s\n", account2.getName());
		account2.deposit(.30);
		
		printAccounts(account1, account2);
		
	}
}