package no.ntnu.imt3281.ch7;

import java.util.ArrayList;

public class ArrayListDemo {
	static void display (ArrayList<String> items, String header) {
		System.out.println(header);
		for (String item : items) {
			System.out.printf("%s ", item);
		}
		System.out.println("\n");
	}
	
	public static void main(String[] args) {
		ArrayList<String> items = new ArrayList<String>();
		
		items.add("red");
		items.add(0, "yellow");
		display(items, "Initial list");
		
		items.add("green");
		items.add("yellow");
		display(items, "List with four elements");
		
		items.remove("yellow");
		display(items, "First yellow removed");
		
		items.remove(1);
		display(items, "Element at index 1 removed");
		
		System.out.printf("\"red\" is %sin the list\n\n", 
				items.contains("red")?"":"not ");
		
		System.out.printf("There are %d items in the list", 
				items.size());
	}
}