package no.ntnu.imt3281.ch7;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestingArrays {

	@Test
	public void theBasics() {
		int numbers[] = new int[10];
		assertEquals(10, numbers.length);	// Correct length
		assertEquals(0, numbers[0]);		// Numbers get initialized to 0
		
		String text[] = new String[10];
		assertEquals(null, text[0]);		// Strings get initialized to null
		
		// numbers = {3, 7, 42};			// Error, can not redeclare this way
		numbers = new int[3];
		
		int numbers1[] = {3, 7, 42};
		assertEquals(42, numbers1[2]);		// All arrays are zero indexed		
	}
	
	// ======================================
	
	@Test
	public void passByReference() {
		int numbers[] = { 3, 4, 5 };
		alteredInMethod(numbers);
		assertEquals(6, numbers[1]);	// Changed
	}

	private void alteredInMethod(int[] numbers) {
		numbers[1] = 6;
	}
	
	// ================================
	
	@Test
	public void referenceNotChanged() {
		int numbers[] = { 3, 4, 5 };
		alterReferenceInMethod(numbers);
		assertEquals(4, numbers[1]);	// Unchanged
	}

	private void alterReferenceInMethod(int[] numbers) {
		numbers = new int[6];
	}

	// ================================
	@Test
	public void cloningVSduplicatingRefences() {
		int numbers[] = { 3, 4, 5 };
		int numbers1[] = numbers;
		numbers1[1] = 42;
		assertEquals (42, numbers[1]);
		assertTrue (numbers==numbers1);	// The references are the same
		
		int numbers2[] = numbers.clone();
		numbers2[1] = 4;
		assertTrue(numbers2[1]!=numbers[1]);
		assertTrue(numbers2!=numbers);	// The references are different
	}
	
	// ================================
	@Test
	public void variableNumberArguments() {
		calledWithThreeArguments(1, 2, 3);
		lastArgumentIs23(1, 2, 3, 23);
	}
	
	private void calledWithThreeArguments(int... a) {
		assertEquals (3, a.length);
	}

	private void lastArgumentIs23(int...a) {
		assertEquals (23, a[a.length-1]);
	}
}
