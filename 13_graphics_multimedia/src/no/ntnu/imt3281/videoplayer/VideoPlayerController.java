package no.ntnu.imt3281.videoplayer;

import java.io.File;
import java.net.MalformedURLException;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.FileChooser;
import javafx.util.Duration;

public class VideoPlayerController {

    @FXML private MediaView mediaView;
    @FXML private BorderPane borderPane;
    @FXML private Button playPauseButton;
    
    private boolean playing = false;
    private MediaPlayer player;

    @FXML
    void playPause(ActionEvent event) {
    	playing = !playing;
    	
    	if (playing) {
    		playPauseButton.setText("Pause");
    		player.play();
    	} else {
    		playPauseButton.setText("Play");
    		player.pause();
    	}
    }

    @FXML
    void selectVideo(ActionEvent event) throws MalformedURLException {
    	FileChooser fileChooser = new FileChooser();
    	fileChooser.setTitle("Velg fil");
    	
    	fileChooser.setInitialDirectory(new File("."));
    	File f = fileChooser.showOpenDialog(borderPane.getScene().getWindow());
    	
		Media media = new Media(f.toURI().toURL().toExternalForm());
		player = new MediaPlayer(media);
		mediaView.setMediaPlayer(player);
		
		player.setOnEndOfMedia(()->{
			playPauseButton.setText("Play");
			player.seek(Duration.ZERO);
			player.pause();			
		});
		
		player.setOnError(()->{
			
		});
		
		player.setOnReady(()->{
			DoubleProperty width = mediaView.fitWidthProperty();
			DoubleProperty height = mediaView.fitHeightProperty();
			width.bind(Bindings.selectDouble(mediaView.sceneProperty(), "width"));
			height.bind(Bindings.selectDouble(mediaView.sceneProperty(), "height"));
			borderPane.getScene().getWindow().setWidth(media.getWidth());
			borderPane.getScene().getWindow().setHeight(media.getHeight());
		});
    }
}
