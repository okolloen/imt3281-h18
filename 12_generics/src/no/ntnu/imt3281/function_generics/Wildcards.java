package no.ntnu.imt3281.function_generics;

import java.util.ArrayList;
import java.util.List;

public class Wildcards {
	public static void main(String[] args) {
		Number[] numbers = { 1, 2.4, 3, 4.2 };
		
		List<Number> numberList = new ArrayList<>();
		for (Number element : numbers) {
			numberList.add(element);
		}
		
		System.out.printf("numberList inneholder : %s%n", numberList);
		System.out.printf("Summen av tallene i numberList er : %.1f%n", sum(numberList));
		
		Double[] doubles = { 1.0, 2.4, 3.0, 4.2 };
		List<Double> doubleList = new ArrayList<>();
		for (Double element : doubles) {
			doubleList.add(element);
		}
		
		System.out.printf("%ndoubleList inneholder : %s%n", doubleList);
		// System.out.printf("Summen av tallene i numberList er : %.1f%n", sum(doubleList));
		// <? extends Number>
	}

	private static double sum(List<Number> numberList) {
		double total = 0;
		for (Number element : numberList) {
			total += element.doubleValue();
		}
		return total;
	}
}
