package no.ntnu.imt3281.function_generics;

public class ExtendingType {
	public static void main(String[] args) {
		System.out.printf("Maksimum av %d, %d og %d er %d%n", 3, 4, 5, 
				maximum(3, 4, 5));
		System.out.printf("Maksimum av %.1f, %.1f og %.1f er %.1f%n", 3.3, 4.4, 5.5, 
				maximum(3.3, 4.4, 5.5));
		System.out.printf("Maksimum av %s, %s og %s er %s%n", "Java", "C++", "Fortran", 
				maximum("Java", "Fortran", "C++"));		
	}
	
	public static <T extends Comparable<T>> T maximum (T x, T y, T z) {
		T max = x;
		if (y.compareTo(max)>0) {
			max = y;
		}
		if (z.compareTo(max)>0) {
			max = z;
		}
		return max;
	}
}
