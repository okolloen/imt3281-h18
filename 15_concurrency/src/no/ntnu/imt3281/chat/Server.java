package no.ntnu.imt3281.chat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

/**
 * Implements a chat server and functions as a controller for Server.fxml.

 * @author oeivindk
 *
 */
public class Server {
    @FXML private TextArea connectionReceived;
    @FXML private TextArea messageReceived;
    @FXML private TextArea disconnectReceived;
    @FXML private Label activeClients;
    @FXML private Label messagesReceived;
    @FXML private Label connectionsReceived;
    @FXML private Label connectionsClosed;
    @FXML private ListView<String> activeClientNames;
    private final ObservableList<String> clientNames = FXCollections.observableArrayList();
    private final Logger logger = Logger.getLogger("Chat server");
    private boolean shutdown = false;
    
    private final ConcurrentHashMap<String, Client> clients = new ConcurrentHashMap<>();
    private final LinkedBlockingQueue<String> messages = new LinkedBlockingQueue<>();
    
    /**
     * Initialize the labels containing values and sets the observable list
     * containing the values for the listview.
     */
    @FXML 
    public void initialize() {
    	activeClients.setText("0");
    	messagesReceived.setText("0");
    	connectionsReceived.setText("0");
    	connectionsClosed.setText("0");
    	activeClientNames.setItems(clientNames);
    }
    
    /**
     * Starts the server, creates the serversocket and the threads 
     * listening for incomming connections, messages from clients 
     * and the thread that sends messages to all clients.
     */
    public Server() {
    	ExecutorService executor = Executors.newCachedThreadPool();
    	executor.execute(()->connectionListenerThread());	// This thread listens for connections from clients
    	executor.execute(()->messageSenderThread());					// This thread listens for messages from clients
    	executor.execute(()->messageListenerThread());					// This thread waits for messages to send to clients, then sends the message(s) to all clients.
    }
    
    /**
     * Waits for connections (on the serversocket) and adds clients to the hashmap.
     */
	private void connectionListenerThread() {
		try (ServerSocket serverSocket = new ServerSocket(Config.SERVERPORT)) {
			while (!shutdown) {		// Run until server stopping
				Socket s= null;
				try {
					s = serverSocket.accept();
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Error while getting client connection: "+Config.SERVERPORT, e);
					System.exit(0);
				}
				try {
					addClient(s);	// Method that gets username and adds client to hashmap
				} catch (IOException e) {
					logger.log(Level.FINER, "Unable to establish connection with client", e);
				}
			}
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Unable to listen to port: "+Config.SERVERPORT, e);
			System.exit(0);
		}
	}

	/**
	 * Watches the message queue and send messages to the clients
	 */
	private void messageSenderThread() {
		while (!shutdown) {
			try {
				final String message = messages.take();		// Blocks until a message is available
				clients.forEachValue(100, client->client.write(message));	// Do in parallel if more than 100 clients
			} catch (InterruptedException e) {
				logger.log(Level.INFO, "Error fetching message from que", e);
				Thread.currentThread().interrupt();
			}
			// Clients that have IO error was marked inactive 
			clients.forEachValue(100, client-> {	// Remove clients that could not receive message
				if (!client.active&&clients.remove(client.getName())!=null) {	// Client is inactive and still existed in the hashmap
					clientRemoved(client);
				}
			});
		}
	}
	
	/**
	 * Listens for messages from clients and adds them to the message queue
	 */
    private void messageListenerThread() {
		while (!shutdown) {
			clients.forEachValue(100, client-> {	
				String msg = client.read();		// Read message from client
				if (msg!=null&&msg.equals("§§BYEBYE§§")) {	// Client says goodbye
					msgFromClient(client, msg);		// Handled separately, adds message to textarea
					if (clients.remove(client.getName())!=null) {
						clientRemoved(client);		// Let other clients know this client has left, adds message to textarea
					}
				} else if (msg!=null) {
					msgFromClient(client, msg);
					messages.add("MSG:"+client.getName()+">"+msg);	// Add message to message queue
				}
			});
			try {
				Thread.sleep(10);	// Prevent excessive processor usage
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}

    /**
     * Used to add message to the textarea showing messages from clients
     * 
     * @param client what client sent the message
     * @param msg what was the message
     */
	private void msgFromClient(Client client, String msg) {
		Platform.runLater(()->{
			messageReceived.setText(String.format("%s%s sent : %s%n", 
					messageReceived.getText(),
					client.getName(),
					msg));
		});
	}

	/**
	 * Called when client is removed, notfies other clients that this client has left
	 * and updated the textarea showing the this client has left.
	 * 
	 * @param client what client has left
	 */
	private void clientRemoved(Client client) {
		messages.add("DISCONNECTED:" +client.getName());	// Message will be sent to all other clients
		Platform.runLater(()->{		// ALL UPTDATES OF THE GUI MUST BE DONE THIS WAY
			clientNames.remove(client.getName());
			activeClients.setText(Integer.toString(Integer.parseInt(activeClients.getText())-1));
			connectionsClosed.setText(Integer.toString(Integer.parseInt(connectionsClosed.getText())+1));
			disconnectReceived.setText(String.format("%sConnection to %s on port %d closed.%n",
					disconnectReceived.getText(),
					client.getName(), 
					client.getSocket().getPort()));
		});
	}

	/**
	 * Called when a socket was accepted from the serversocket, will create a
	 * Client object and add it the clients map. Will also send a message
	 * to already connected clients informing them that a new client has joined
	 * and update the textarea showing new connections.
	 * 
	 * @param s the socket for this connection
	 * @throws IOException if  unable to read from the socket.
	 */
	private void addClient(Socket s) throws IOException {
		final Client client = new Client(s);
		clients.forEachKey(100, name-> client.write("JOIN:"+name)); // Let the new client know the name of the existing clients
		clients.put(client.getName(), client);
		messages.add("JOIN:" +client.getName());
		Platform.runLater(()->{
			clientNames.add(client.getName());
			activeClients.setText(Integer.toString(Integer.parseInt(activeClients.getText())+1));
			connectionsReceived.setText(Integer.toString(Integer.parseInt(connectionsReceived.getText())+1));
			connectionReceived.setText(String.format("%sConnection received from %s on port %d. Logged on as : %s%n", 
					connectionReceived.getText(), 
					client.getSocket().getInetAddress().getHostName(),
					client.getSocket().getPort(),
					client.getName()));
		});
	}

	/**
	 * Holds information about a client, ie the connection information and the
	 * name of the user.
	 * 
	 * @author oeivindk
	 *
	 */
	class Client {
    	private String name;
    	private Socket s;
    	private BufferedReader input;
    	private BufferedWriter output;
    	private boolean active = true;
    	
    	/**
    	 * Gets a socket and creates reader/writer objects and reads the name of the client/user
    	 * 
    	 * @param s the socket from the serversocket
    	 * @throws IOException if unable to create reader/writer objects or read username.
    	 */
    	public Client (Socket s) throws IOException {
    		this.s = s;
    		input = new BufferedReader(new InputStreamReader(s.getInputStream()));
    		output = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
    		name = input.readLine();
    	}
    	
    	/**
    	 * Non blocking read of the input reader.
    	 * Checks if data is available and if so reads the data.
    	 * 
    	 * @return the data read from the client or null if no data was available.
    	 */
    	public String read() {
    		try {
				if (input.ready()) {
					return input.readLine();
				} else {
					return null;
				}
			} catch (IOException e) {
				active = false;	// If unable to read (IO error) mark as inactive, this will remove the client from the server
				return null;
			}
    	}
    	
    	public void write(String msg) {
    		try {
				output.write(msg);
				output.newLine();	// Must send newline for client to be able to read
				output.flush();		// Nothing is sent without flushing
			} catch (IOException e) {
				active = false; // If unable to write (IO error) mark as inactive, this will remove the client from the server
			}
    	}
    	
    	public boolean getActive() {
    		return active;
    	}
    	
    	public String getName() {
    		return name;
    	}
    	
    	public Socket getSocket() {
    		return s;
    	}
    	
    	/**
    	 * Marks this client as inactive and closes the connection.
    	 */
    	public void close() {
    		try {
    			active = false;
				input.close();
				output.close();
				s.close();
			} catch (IOException e) {
				// This connection will be dropped anyway, nothing much to do about it
			}
    	}
    }
}
