package no.ntnu.imt3281.createLayoutInCode;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class Controller {

    @FXML
    private BorderPane borderPane;

    @FXML
    private TextField rows;

    @FXML
    private TextField cols;
    
    @FXML
    private TextField status;

    @FXML
    void generate(ActionEvent event) {
    	int numRows = Integer.parseInt(rows.getText());
    	int numCols = Integer.parseInt(cols.getText());
    	GridPane grid = new GridPane();		// Create container
    	TextField textFields[][] = new TextField[numRows][numCols];	
    	for (int r=0; r<numRows; r++) {
    		for (int c=0; c<numCols; c++) {
    			grid.add(textFields[r][c] = new TextField(String.format("[%d][%d]", r, c)), 
    					c, r);	// Create TextField and add to container
    			
    			final int col = c;	// Must be defined final to be used in listener
    			final int row = r;	// 
    			
    			textFields[r][c].textProperty().addListener(new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue,
							String newValue) {
						status.setText(String.format("TextField at row %d, col %d changed to %s",  row, col, newValue));
					}
    			});
    		}
    	}
    	borderPane.setCenter(grid);	// Add (set) container as center of borderpane
    }
}
