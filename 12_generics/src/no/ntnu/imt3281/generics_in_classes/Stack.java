package no.ntnu.imt3281.generics_in_classes;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Stack<T> {
	private final ArrayList<T> elements;
	
	public Stack() {
		this(10);
	}
	
	public Stack(int size) {
		size = size>=0?size:10;
		elements = new ArrayList<>(10);
	}
	
	public void push(T value) {
		elements.add(value);
	}
	
	public T pop() {
		if (elements.isEmpty()) {
			throw new NoSuchElementException("Stack is empty, cannot pop!");
		}
		return elements.remove(elements.size()-1);
	}
}
