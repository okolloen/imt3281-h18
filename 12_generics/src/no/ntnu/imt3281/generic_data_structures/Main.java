package no.ntnu.imt3281.generic_data_structures;

import java.util.NoSuchElementException;

public class Main {
	public static void main(String args[]) {
		MyIterator<String> strings = new MyIterator<>("Hallo", "verden", "!");
		while (strings.hasNext()) {
			System.out.printf("%s ", strings.next());
		}
		System.out.println("\n---------");
		
		MyIterator<Integer> ints= new MyIterator<>(1, 2, 3, 4, 5, 6, 7);
		while (ints.hasNext()) {
			System.out.printf("%d ", ints.next());
		}
		try {
			ints.next();
		} catch (NoSuchElementException nsee) {
			System.out.printf("%nOppss, det var tomt ja : %s%n", nsee.getMessage());
		}
	}
}
