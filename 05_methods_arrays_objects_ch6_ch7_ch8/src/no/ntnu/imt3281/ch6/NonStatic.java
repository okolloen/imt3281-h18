package no.ntnu.imt3281.ch6;

import java.util.Scanner;

public class NonStatic {
	static String formatString = "Det største taller er %.4f";
	
	double maximum (double x, double y, double z) {
		return (x>y)
				?(x>z)?x:z
				:(y>z)?y:z;
	}
	
	public NonStatic() {
		System.out.println("Skriv inn tre desimaltall, skill de med mellomrom:");
		Scanner input = new Scanner(System.in);
		double number1 = input.nextDouble();
		double number2 = input.nextDouble();
		double number3 = input.nextDouble();
		double result = maximum (number1, number2, number3);
		
		System.out.printf(NonStatic.formatString, result);
		input.close();
	}
	
	public static void main(String[] args) {
		new NonStatic();
	}
}
