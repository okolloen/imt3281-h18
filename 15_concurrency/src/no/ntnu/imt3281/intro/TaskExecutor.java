package no.ntnu.imt3281.intro;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaskExecutor {
	public static void main(String[] args) {
		PrintTask task1 = new PrintTask("Task 1");
		PrintTask task2 = new PrintTask("Task 2");
		PrintTask task3 = new PrintTask("Task 3");
		
		System.out.println("Starting executor");
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		executor.execute(task1);
		executor.execute(task2);
		executor.execute(task3);
		
		executor.shutdown();
		System.out.println("Tasks started, main ends.\n");
	}
}
