package no.ntnu.imt3281.inheritance;

public class A {
	
	public A () {
		System.out.println("Constructor of A was called");
	}
	
	@Override
	public String toString() {
		return String.format("I'm %s and my parent is : %s", getClass().getName(), super.toString());
	}
}
