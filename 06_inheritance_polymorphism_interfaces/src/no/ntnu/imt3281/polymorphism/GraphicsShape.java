package no.ntnu.imt3281.polymorphism;

import javafx.scene.canvas.GraphicsContext;

public interface GraphicsShape {
	public void draw(GraphicsContext gc);
}