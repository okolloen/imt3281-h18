package no.ntnu.imt3281.socket_echo_server;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

public class SocketEchoServerController {

    @FXML private TextArea textArea;
    
    public void appendToTextArea (String msg) {
    	Platform.runLater(()-> {	// Update GUI on GUI thread
    		textArea.setText(textArea.getText()+msg+"\n");
    	});
    }
}

