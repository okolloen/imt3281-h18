package no.ntnu.imt3281.polymorphism;

public class Demo {
	public static void main(String[] args) {
		Person people[] = new Person[4];
		people[0] = new Student("Ole", "Olsen", "NTNU", "Bachelor i programmering");
		people[1] = new AcademicEmployee("Kari", "Knudsen", "NTNU", "Professor");
		people[2] = new Student("Mari", "Monsen", "UiO", "Antikkens kultur");
		people[3] = new AcademicEmployee("Kåre", "Nordmann", "UiO", "Dosent");
		
		for (Person p: people) {
			System.out.println(p);
		}
		
		for (Person p: people) {
			if (p instanceof Student) {
				((Student) p).setProgram("Master i programmering");
			}
		}
		
		for (Person p: people) {
			System.out.println(p.getGivenName()+", "+p.getClass().getSimpleName());
		}
	}
}
