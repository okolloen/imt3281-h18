package drawLinesComplex;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class DrawLinesController {

    @FXML
    private Canvas canvas;

    @FXML
    void drawLinesButtonPressed(ActionEvent event) {
    	GraphicsContext gc = canvas.getGraphicsContext2D();
    	gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    	gc.strokeLine(0, 0, canvas.getWidth(), canvas.getHeight());
    	gc.strokeLine(canvas.getWidth(), 0, 0, canvas.getHeight());
    }
    
    @FXML
    void drawLinesCurvedAllCornersButtonPressed(ActionEvent event) {
    	GraphicsContext gc = canvas.getGraphicsContext2D();
    	gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    	int step = 0;
    	while (step<21) {
    		// Curved bottom left
    		int x1 = 0;
    		int y1 = step*((int)canvas.getHeight()/20);
    		int x2 = step*((int)canvas.getWidth()/20);
    		int y2 = (int)canvas.getHeight();
    		gc.strokeLine(x1, y1, x2, y2);
    		
    		// Curved bottom right
    		x1 = (int)canvas.getWidth();
    		y1 = step*((int)canvas.getHeight()/20);
    		x2 = (int)canvas.getWidth()-x2;
    		gc.strokeLine(x1, y1, x2, y2);
    		
    		// Curved top right
    		y1 = (int)canvas.getHeight()-y1;
    		y2 = 0;
    		gc.strokeLine(x1, y1, x2, y2);
    		
    		// Curved top Left
    		x1 = 0;
    		x2 = step*((int)canvas.getWidth()/20);
    		gc.strokeLine(x1, y1, x2, y2);
    		
    		step++;
    	}
    }

    @FXML
    void drawLinesCurvedButtonPressed(ActionEvent event) {
    	GraphicsContext gc = canvas.getGraphicsContext2D();
    	gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    	int step = 0;
    	while (step<20) {
    		// Curved bottom left
    		int x1 = 0;
    		int y1 = step*((int)canvas.getHeight()/20);
    		int x2 = step*((int)canvas.getWidth()/20);
    		int y2 = (int)canvas.getHeight();
    		gc.strokeLine(x1, y1, x2, y2);
    		step++;
    	}
    }

    @FXML
    void drawLinesFromAllCornersButtonPressed(ActionEvent event) {
    	GraphicsContext gc = canvas.getGraphicsContext2D();
    	gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    	int step = 0;
    	while (step<20) {
    		// Drawing lines from top left corner
    		int x = step*((int)canvas.getWidth()/20);
    		int y = (20-step)*((int)canvas.getHeight()/20);
        	gc.strokeLine(0, 0, x, y);

        	// Draw lines from bottom right corner
        	x = (int)canvas.getWidth()-x;
        	y = (int)canvas.getHeight()-y;
        	gc.strokeLine(canvas.getWidth(), canvas.getHeight(), x, y);
        	
        	// Draw lines from top right corner
        	x = step*((int)canvas.getWidth()/20);
        	y = step*((int)canvas.getHeight()/20);
        	gc.strokeLine(canvas.getWidth(), 0, x, y);
    		
        	// Draw lines from bottom left corner
        	x = (int)canvas.getWidth()-x;
        	y = (int)canvas.getHeight()-y;
        	gc.strokeLine(0, canvas.getHeight(), x, y);
    		
        	step++;
    	}
    }

    @FXML
    void drawLinesFromCornerButtonPressed(ActionEvent event) {
    	GraphicsContext gc = canvas.getGraphicsContext2D();
    	gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    	int step = 0;
    	while (step<20) {
    		int x = step*((int)canvas.getWidth()/20);
    		int y = (20-step)*((int)canvas.getHeight()/20);
        	gc.strokeLine(0, 0, x, y);   
        	step++;
    	}
    }

}	
