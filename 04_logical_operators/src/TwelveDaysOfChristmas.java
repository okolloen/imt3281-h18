
public class TwelveDaysOfChristmas {
	
	public static void main(String[] args) {
		String days[] = {"First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eigth", "Ninth", "Thenth", "Eleventh", "Twelfth"};

		for (int i=0; i<12; i++) {
			System.out.printf("On the %s day of Christmas my true love sent to me\n", days[i]);
			switch (i) {
				case 11 :
					System.out.println("Twelve Drummers Drumming");
				case 10 :
					System.out.println("Eleven Pipers Piping");
				case 9 :
					System.out.println("Ten Lords a-Leaping");
				case 8 : 
					System.out.println("Nine Ladies Dancing");
				case 7 : 
					System.out.println("Eight Maids a-Milking");
				case 6 :
					System.out.println("Seven Swans a-Swimming");
				case 5 :
					System.out.println("Six Geese a-Laying");
				case 4 :
					System.out.println("Five Gold Rings");
				case 3 :
					System.out.println("Four Calling Birds");
				case 2 : 
					System.out.println("Three French Hens");
				case 1 : 
					System.out.println("Two Turtle Doves");
				case 0 :
					System.out.printf("%s Partridge in a Pear Tree.\n\n", (i==0?"a":"and"));
			}
		}
	}
}
