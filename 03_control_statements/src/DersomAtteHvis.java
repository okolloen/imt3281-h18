import java.util.Scanner;

public class DersomAtteHvis {
	public static void main(String[] args) {
		int a = 10, b = 20;
		
		// If
		if (a<b) {
			System.out.println("Jada");
		}
		
		// If else
		if (a>b) {
			System.out.println("Neida");
		} else {
			System.out.println("Jada");
		}
		
		// If/else if/else
		int c = 30;
		if (a>b) {
			System.out.println("Neida");
		} else if (a>c) {
			System.out.println("Neida");
		} else {
			System.out.println("Javist");
		}
		
		// Conditional operator (?:)
		System.out.println((a > b) ? "A>B" : "A<=B");
		
		// Counter controlled iteration
		int counter=0;
		while (counter<3) {
			System.out.println(counter + "\n");
			counter++;
		}
		
		// Senteniel controlled iteration
		Scanner input = new Scanner(System.in);
		int total = 0, number = 0, count = 0;
		do {
			System.out.println("Skriv inn tall du ønsker å finne gjennomsnittet av, -1 for å avslutte :");
			number = input.nextInt();
			if (number!=-1) {
				total += number;
				count++;
			}
		} while (number!=-1);
		double avg = total*1.0f/count;
		System.out.printf("Gjennomsnittet av de %i tallene du skrev inn er : $0.2f", count, avg);
		input.close();
	}
}
