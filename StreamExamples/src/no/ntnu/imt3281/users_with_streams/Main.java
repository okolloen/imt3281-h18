package no.ntnu.imt3281.users_with_streams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Demo program used to read data from a comma separated file and
 * display part of this dataset (female users, with email address 
 * from the .com domain.)
 * 
 * @author oeivindk
 *
 */
public class Main {
	public static void main(String[] args) throws IOException {
		try (Stream<String> input = Files.lines(Paths.get("MOCK_DATA.csv"))) {
			input.filter(line->!line.startsWith("id,"))	// Remove first line
			.map(line -> new User(line))				// Convert to User objects
			.filter(user -> user.getGender().equals("Female") &&
				user.getEmail().endsWith(".com"))
			.sorted(Comparator.comparing(User::getLast))
			.forEach(user -> { 							// Print out information
				System.out.printf("%s, %s - %s%n", 
					user.getLast(), 
					user.getFirst(), 
					user.getEmail()); 
			});
		} catch (IOException ioe) {
			Logger.getLogger("StreamDemo").log(Level.WARNING, 
					"Error reading file", ioe);
		}
	}
}