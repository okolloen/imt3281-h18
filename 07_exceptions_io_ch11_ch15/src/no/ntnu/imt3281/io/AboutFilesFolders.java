package no.ntnu.imt3281.io;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AboutFilesFolders {

	public static void main(String[] args) throws IOException {
		Path path = Paths.get(".");
		if (Files.exists(path)) {
			System.out.printf("%s a directory\n", Files.isDirectory(path) ? "Is" : "Is not");
			System.out.printf("%s an absolute path\n", path.isAbsolute() ? "Is" : "Is not");
			System.out.printf("Last modified: %s\n", Files.getLastModifiedTime(path));
			System.out.printf("Size: %s\n", Files.size(path));
			System.out.printf("Path: %s\n", path);
			System.out.printf("Absolute path: %s\n", path.toAbsolutePath());
			
			if (Files.isDirectory(path)) {
				System.out.println("\nDirectory contents:\n");
				
				DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path);
				for (Path p : directoryStream) {
					System.out.println(p);
				}
			}
		} else {
			System.out.println("'.' finnes ikke."); 
		}

	}

}
