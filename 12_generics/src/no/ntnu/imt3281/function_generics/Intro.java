package no.ntnu.imt3281.function_generics;

public class Intro {
	public static void main(String[] args) {
		Integer[] ints = { 1, 2, 3, 4, 5, 6 };
		Double[] floats = { 1.2, 3.4, 5.6, 7.8, 9.12 };
		String[] strings = { "Hei", "Hallo", "Goddag", "Adjø" };
		
		printArray(ints);
		printArray(floats);
		printArray(strings);
	}

	private static <T> void printArray(T[] values) {
		System.out.printf("Array av %s inneholder : ", values[0].getClass().getSimpleName());
		for (T element : values) {
			System.out.printf("%s ", element);
		}
		System.out.println();
	}
}