package no.ntnu.imt3281.socket_reader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class WebBrowserController {

    @FXML private TextField address;
    @FXML private TextArea textArea;
    
    @FXML
    void browse(ActionEvent event) {
    	try {
			URL url = new URL(address.getText());
			int port = url.getPort()!=-1?url.getPort():80;
			
			Socket s = new Socket(url.getHost(), port);
			
			String file = url.getPath()!=""?url.getPath():"/";
			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
			bw.write("GET "+file+" HTTP/1.1\r\n");		// Note \r AND \n
			bw.write("Host: "+url.getHost()+"\r\n");	// Note \r AND \n
			bw.write("\r\n");							// Empty line ends the request
			bw.flush();									// Send now
			StringBuffer sb = new StringBuffer();
			String tmp;
			while ((tmp=br.readLine())!=null) {
				sb.append(tmp);
				sb.append("\n");
			}
			textArea.setText(sb.toString());
			br.close();
			bw.close();
			s.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

