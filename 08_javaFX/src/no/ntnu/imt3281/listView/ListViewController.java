package no.ntnu.imt3281.listView;

import java.net.URL;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

public class ListViewController {

    @FXML
    private ListView<Browser> list;

    @FXML
    private ImageView image;
    
    private final ObservableList<Browser> browsers = 
    		FXCollections.observableArrayList();
    
    @FXML
    public void initialize() {
    	browsers.add(new Browser("Chrome", "images/chrome.jpg"));
    	browsers.add(new Browser("Edge", "images/edge.jpg"));
    	browsers.add(new Browser("FireFox", "images/firefox.jpg"));
    	browsers.add(new Browser("Opera", "images/opera.jpg"));
    	browsers.add(new Browser("Safari", "images/safari.jpg"));
    	
    	list.setItems(browsers);
    	
    	list.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Browser> () {

			@Override
			public void changed(ObservableValue<? extends Browser> observable, Browser oldValue, Browser newValue) {
				image.setImage(new Image(getClass().getResourceAsStream(newValue.getImgPath())));	
			}
    	});
    	
    	list.setCellFactory(new Callback<ListView<Browser>, ListCell<Browser>>() {
			
			@Override
			public ListCell<Browser> call(ListView<Browser> param) {
				return new ImageTextCell();
			}
		});
    }

    
    class Browser {
    	String name, img;
    	
    	public Browser(String name, String path) {
    		this.name = name;
    		this.img = path;
    	}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the img
		 */
		public String getImgPath() {
			return img;
		}
		
		@Override
		public String toString() {
			return name;
		}
    	
    }
}