package no.ntnu.imt3281.exceptions;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.hamcrest.Matchers.is;

public class TestMyException {
	@Rule
	final public ExpectedException exception = ExpectedException.none();

	@Test
	public void testMyException() {
		MyException me = new MyException("Really usefull exception", 42);
		exception.expect(is(me));
		throw new MyException("Really usefull exception", 42);
	}
}
