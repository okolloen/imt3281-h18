package no.ntnu.imt3281.web_browser;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class WebBrowserController {

    @FXML private TextField address;
    @FXML private WebView browser;
    private WebEngine webEngine;

    @FXML
    public void initialize() {
    	webEngine = browser.getEngine();
    	System.out.println(webEngine.getUserAgent());
    }
    
    @FXML
    void browse(ActionEvent event) {
    	webEngine.load(address.getText());
    }
}

