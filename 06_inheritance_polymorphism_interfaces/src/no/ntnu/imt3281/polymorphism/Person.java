package no.ntnu.imt3281.polymorphism;

public abstract class Person {
	String givenName, sureName;
	
	public Person (String givenName, String sureName) {
		this.givenName = givenName;
		this.sureName = sureName;
	}

	public String getGivenName() {
		return givenName;
	}

	public String getSureName() {
		return sureName;
	}
}
