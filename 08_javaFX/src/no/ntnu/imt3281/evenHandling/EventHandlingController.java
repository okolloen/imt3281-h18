package no.ntnu.imt3281.evenHandling;

import java.text.NumberFormat;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class EventHandlingController {
	private static final NumberFormat percentFormatter = NumberFormat.getPercentInstance();
	private static final NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();

	@FXML
    private Button button3;

    @FXML
    private TextField input;

    @FXML
    private Slider slider;

    @FXML
    private TextField value;

    @FXML
    private TextArea output;

    @FXML
    void button1(ActionEvent event) {
    	output.setText(output.getText()+String.format("%s\n", "Button 1 ble trykket"));
    }

    @FXML
    void button2(ActionEvent event) {
    	output.setText(output.getText()+String.format("%s\n", "Button 2 ble trykket"));
    }

    @FXML
    void textAction(ActionEvent event) {
    	output.setText(output.getText()+String.format("ActionEvent i tekstfeltet : %s\n", ((TextField)event.getSource()).getText()));
    }
    
    @FXML
    public void initialize() {
    	slider.valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				String percent = percentFormatter.format(newValue.intValue()/100.0);
				String currency = currencyFormatter.format(newValue.intValue()+0.50);
				value.setText(String.format("%d - %s - %s", 
						newValue.intValue(), percent, currency));
			}
    	});
    	
    	input.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				output.setText(output.getText()+String.format("Input text field changed to: %s\n", newValue));
			}
    	});
    	
    	button3.setOnAction(new EventHandler<ActionEvent>() {	
			@Override
			public void handle(ActionEvent event) {
				output.setText(output.getText()+String.format("%s\n", "Button 3 ble trykket"));				
			}
		});
    }

}