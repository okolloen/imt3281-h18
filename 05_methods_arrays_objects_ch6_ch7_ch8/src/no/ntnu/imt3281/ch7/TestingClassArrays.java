package no.ntnu.imt3281.ch7;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class TestingClassArrays {

	@Test
	public void testSorting() {
		double numbers[] = { 8.4, 0.1, 2.3, 7.9, 3.14 };
		Arrays.sort(numbers);
		assertEquals(0.1, numbers[0], 0);
		assertEquals(8.4, numbers[numbers.length-1], 0);		
	}
	
	@Test
	public void testFillAndEquals() {
		int sevens[] = { 7, 7, 7, 7, 7 };
		int empty[] = new int[5];
		Arrays.fill(empty, 7);
		assertTrue (Arrays.equals(sevens, empty));
	}

	@Test
	public void testBinarySearch() {
		int numbers[] = { 1, 2, 3, 4, 6, 7, 8, 9 };
		assertEquals (?, Arrays.binarySearch(numbers, 4));
		assertEquals (?, Arrays.binarySearch(numbers, 5));
	}
}
