package no.ntnu.imt3281.css_styling;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("FontsCSS.fxml"));
		Scene scene = new Scene(root);
		// scene.getStylesheets().add(getClass().getResource("fonts.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle("Styling with CSS");
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
