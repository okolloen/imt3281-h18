package no.ntnu.imt3281.math;

/**
 * fibonaci(0) = 0
 * fibonaci(1) = 1
 * fibonaci(n) = fibonaci(n-1)+fibonaci(n-2)
 * 
 * @author oeivindk
 *
 */
public class FibonacciCalculator {
	static int callsToFibonacci = 0;
	
	public static long fibonacci(int n) {
		callsToFibonacci++;
		if (n<2) {
			return n;
		} else {
			return fibonacci(n-1)+fibonacci(n-2);
		}
	}
	
	public static void main(String[] args) {
		long start = System.nanoTime();
		for (int i=0; i<35; i++) {
			callsToFibonacci=0;
			System.out.printf("Fibonacci av %d er : %d, fibinacci metoden er kallt %d ganger\n", i, fibonacci(i), callsToFibonacci);
			//fibonacci(i);
		}
		System.out.printf("Dette tok %.2f millisekunder\n", (System.nanoTime()-start)/1000000.0);
		
		start = System.nanoTime();
		for (int i=0; i<35; i++) {
			System.out.printf("Fibonacci av %d er : %d\n", i, fibonacciNonRecursive(i));
			//fibonacciNonRecursive(i);
		}
		System.out.printf("Dette tok %.2f millisekunder\n", (System.nanoTime()-start)/1000000.0);
	}
	
	public static long fibonacciNonRecursive(int n) {
		if (n<2) {
			return n;
		} else {
			int n2 = 0;	// n - 2
			int n1 = 1; // n - 1
			int f = 0;
			for (int i=2; i<=n; i++) {
				f = n1 + n2;
				n2 = n1;
				n1 = f;
			}
			return f;
		}
	}
}
