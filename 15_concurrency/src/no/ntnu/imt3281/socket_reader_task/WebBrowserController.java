package no.ntnu.imt3281.socket_reader_task;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class WebBrowserController {

    @FXML private TextField address;
    @FXML private TextArea textArea;
    @FXML private Label status;
    
    @FXML
    void browse(ActionEvent event) {
    	try {
			URL url = new URL(address.getText());
			GetPageContents fetcher = new GetPageContents(url);
			status.textProperty().bind(fetcher.messageProperty());
			fetcher.setOnSucceeded((e)->{
				textArea.setText(fetcher.getValue());
			});
			ExecutorService executor = Executors.newFixedThreadPool(1);
			executor.execute(fetcher);
			executor.shutdown();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

class GetPageContents extends Task<String> {
	private final URL address;
	
	public GetPageContents(URL u) {
		address = u;
	}

	@Override
	protected String call() {
		int port = address.getPort()!=-1?address.getPort():80;
		String file = address.getPath()!=""?address.getPath():"/";
		
		updateMessage(String.format("Opening connection to host: %s", address.getHost()));
		try (Socket s = new Socket(address.getHost(), port)) {
			
			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
			updateMessage(String.format("Requesting file: %s", file));
			bw.write("GET "+file+" HTTP/1.1\r\n");		// Note \n AND \r
			bw.write("Host: "+address.getHost()+"\r\n");	// Note \n AND \r
			bw.write("\r\n");							// Empty line ends the request
			bw.flush();									// Send now
			StringBuffer sb = new StringBuffer();
			String tmp;
			while ((tmp=br.readLine())!=null) {
				sb.append(tmp);
				sb.append("\n");
				updateMessage(String.format("Received %d characters", sb.length()));
			}
			br.close();
			bw.close();
			updateMessage(String.format("Done"));
			return sb.toString();
		} catch (Exception e) {
			updateMessage(String.format("Unable to connect to : %s", address.getHost()));
		}
		return "";
	}
}
