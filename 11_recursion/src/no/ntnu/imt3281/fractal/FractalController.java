package no.ntnu.imt3281.fractal;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

public class FractalController {
	private static final int MIN_LEVEL=0;
	private static final int MAX_LEVEL=15;

    @FXML private Label levelLabel;
    @FXML private Canvas canvas;
    @FXML private ColorPicker colorPicker;
    
    private Color currentColor = Color.BLUE;
    private int level = MIN_LEVEL;
    private GraphicsContext gc;
    
    @FXML
    public void initialize() {
    	levelLabel.setText("Level: "+level);
    	colorPicker.setValue(currentColor);
    	gc = canvas.getGraphicsContext2D();
    	drawFractal();
    }
    
    private void drawFractal() {
		gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		gc.setStroke(currentColor);
		drawFractal(level, 0, 600, 500	, 0);
	}

	private void drawFractal(int level, double x1, double y1, double x2, double y2) {
		if (level==0) {
			gc.strokeLine(x1, y1, x2, y2);
		} else {
			Line l = new  Line (x1, y1, x2, y2);
			l.setLength(l.getLength()/3);
			drawFractal (level-1, l.getX1(), l.getY1(), l.getX2(), l.getY2());
			l.setX1(l.getX2());
			l.setY1(l.getY2());
			l.setAngle(l.getAngle()-Math.PI/3);
			drawFractal (level-1, l.getX1(), l.getY1(), l.getX2(), l.getY2());
			l.setX1(l.getX2());
			l.setY1(l.getY2());
			l.setAngle(l.getAngle()+Math.PI/3*2);
			drawFractal (level-1, l.getX1(), l.getY1(), l.getX2(), l.getY2());
			l.setX1(l.getX2());
			l.setY1(l.getY2());
			l.setAngle(l.getAngle()-Math.PI/3);
			drawFractal (level-1, l.getX1(), l.getY1(), l.getX2(), l.getY2());
		}
	}

	@FXML 
    void colorSelected(ActionEvent event) {
		currentColor = colorPicker.getValue();
		drawFractal();
    }

    @FXML
    void decreaseLevel(ActionEvent event) {
    	if (level>MIN_LEVEL) {
    		level--;
    		drawFractal();
    	}
    	levelLabel.setText("Level: "+level);
    }

    @FXML
    void increaseLevel(ActionEvent event) {
    	if (level<MAX_LEVEL) {
    		level++;
    		drawFractal();
    	}
    	levelLabel.setText("Level: "+level);
    }
}

class Line {
	double length, angle;
	double x1, y1;
	
	private void line (double x1, double y1, double length, double angle) {
		this.x1 = x1;
		this.y1 = y1;
		this.length = length;
		this.angle = angle;
	}
	
	public Line (double x1, double y1, double x2, double y2) {
		line(x1, y1, Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)), Math.atan2(y1 - y2, x1 - x2)+Math.PI);
	}
	
	public double getX1() {
		return x1;
	}
	
	public double getX2() {
		return x1 + length*Math.cos(angle);
	}
	
	public double getY1() {
		return y1;
	}
	
	public double getY2() {
		return y1 + length*Math.sin(angle);
	}
	
	public double getLength() {
		return length;
	}
	
	public double getAngle() {
		return angle;
	}
	
	public void setLength(double length) {
		this.length = length;
	}
	
	public void setAngle(double angle) {
		this.angle = angle;
	}
	
	public void setX1 (double x1) {
		this.x1 = x1;
	}
	
	public void setY1 (double y1) {
		this.y1 = y1;
	}
}