package no.ntnu.imt3281.buffers;

public class UnsynchronizedBuffer<T> implements Buffer<T> {
	private T buffer = null;
	
	@Override
	public void put(T value) throws InterruptedException {
		System.out.printf("Lagt i bufferet\t\t%4s%n", value);
		buffer = value;
	}

	@Override
	public T get() throws InterruptedException {
		System.out.printf("Hentet fra bufferet\t%4s%n", buffer!=null?buffer:"");
		return buffer;
	}
}
