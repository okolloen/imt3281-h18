package no.ntnu.imt3281.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.xml.bind.JAXB;

public class readXMLFile {
	public static void main(String[] args) {
		try (BufferedReader br = Files.newBufferedReader(Paths.get("users.xml"))) {
			Users users = JAXB.unmarshal(br, Users.class);
			System.out.printf("%-5s%-20s%-20s%s\n", "id", "First name", "Last name", "ip");
			
			for (User u : users.getUsers()) {
				System.out.printf("%-5s%-20s%-20s%s\n", u.getId(), u.getFirstName(),
						u.getLastName(), u.getIp());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
