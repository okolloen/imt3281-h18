package no.ntnu.imt3281.generics_in_classes;

import java.util.NoSuchElementException;

public class StackTest {
	public static void main (String args[]) {
		Integer ints[] = { 1, 2, 3, 4, 5, 6 };
		String strings[] = { "Hei", "på", "alle", "sammen" };
		
		Stack<Integer> intStack = new Stack<>();
		Stack<String> stringStack = new Stack<String>(5);
		
		testPush(intStack, ints);
		testPop(intStack);
		
		System.out.println();
		
		testPush(stringStack, strings);
		testPop(stringStack);
	}

	private static <T> void testPop(Stack<T> stack) {
		try {
			T popValue = stack.pop();
			System.out.printf("Poping elements of stack of %s%n", 
					popValue.getClass().getSimpleName());
			while (popValue!=null) {
				System.out.printf("Popped : %s%n", popValue);
				popValue = stack.pop();
			}
		} catch (NoSuchElementException nsee) {
			System.out.println();
			nsee.printStackTrace();
		}
	}

	private static <T>void testPush(Stack<T> stack, T[] values) {
		System.out.printf("Pushing elements onto a stack of %s%n",
				values[0].getClass().getSimpleName());
		for (T element : values) {
			System.out.printf("Pushing %s%n", element);
			stack.push(element);
		}
		System.out.println();
	}
}
