package no.ntnu.imt3281.animation;

import java.awt.Point;
import java.security.SecureRandom;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

public class AnimationController {

    @FXML private AnchorPane pane;
    @FXML private Circle c;
    private final Point cSpeed = new Point();
    
    @FXML
    public void initialize() {
    	SecureRandom random = new SecureRandom();
    	
    	cSpeed.x = 1 + random.nextInt(5);
    	cSpeed.y = 1 + random.nextInt(5);
    	
    	Timeline timelineAnimation = new Timeline(
    			new KeyFrame(Duration.millis(10), (e)-> {
    				c.setLayoutX(c.getLayoutX()+cSpeed.x);
    				c.setLayoutY(c.getLayoutY()+cSpeed.y);
    				
    				Bounds bounds = pane.getBoundsInLocal();
    				if (hitLeftOrRight(bounds)) {
    					cSpeed.x *= -1;
    				}
    				if (hitTomOrBottom(bounds)) {
    					cSpeed.y *= -1;
    				}
    			}));
    	timelineAnimation.setCycleCount(Timeline.INDEFINITE);
    	timelineAnimation.play();
    }

	private boolean hitTomOrBottom(Bounds bounds) {
		return (c.getLayoutY()<=(bounds.getMinY()+c.getRadius())) ||
				(c.getLayoutY()>=(bounds.getMaxY()-c.getRadius()));
	}

	private boolean hitLeftOrRight(Bounds bounds) {
		return (c.getLayoutX()<=(bounds.getMinX()+c.getRadius())) ||
				(c.getLayoutX()>=(bounds.getMaxX()-c.getRadius()));
	}
}
