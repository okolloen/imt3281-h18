package no.ntnu.imt3281.polymorphism;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

public class DrawingController {

    @FXML
    private Canvas canvas;

    @FXML
    void initialize() {
    	GraphicsShape shapes[] = new GraphicsShape[5];
    	shapes[0] = Line.createLine(10, 10, 400, 40);
    	shapes[1] = Line.createLine(100, 10, 300, 40);
    	shapes[2] = Line.createLine(10, 100, 200, 40);
    	shapes[3] = Line.createLine(150, 150, 40, 40);
    	shapes[4] = Line.createLine(10, 10, 10, 320);
    	GraphicsContext gc = canvas.getGraphicsContext2D();
    	for (GraphicsShape s : shapes) {
    		s.draw(gc);
    	}
    }
}
