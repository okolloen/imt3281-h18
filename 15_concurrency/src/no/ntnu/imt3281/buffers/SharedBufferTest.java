package no.ntnu.imt3281.buffers;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SharedBufferTest {
	public static void main(String[] args) throws InterruptedException {
		ExecutorService executor = Executors.newCachedThreadPool();
		
		Buffer<Integer> sharedLocation = new BlockingBuffer<>(3);
		System.out.println("Hendelse\t\tVerdi\tSum skrevet\tSum lest");
		System.out.println("--------\t\t-----\t-----------\t--------");
		
		executor.execute(new Producer(sharedLocation));
		executor.execute(new Consumer(sharedLocation));
		
		executor.shutdown();
		executor.awaitTermination(1, TimeUnit.MINUTES);
	}
}
