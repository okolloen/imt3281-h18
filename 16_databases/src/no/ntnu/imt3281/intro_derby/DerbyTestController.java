package no.ntnu.imt3281.intro_derby;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class DerbyTestController {
    @FXML private WebView createDBCode;
    @FXML private WebView createTableCode;
    @FXML private WebView insertDataCode;
    @FXML private WebView queryCode;
    @FXML private WebView queryResult;
    
    private final static String createCode = String.format("<body style='font-size: 28px; font-family: menlo,Verdana,sans-serif'><pre>%s%n%s%n%s%n%s%n%s%n%s%n%s%n</pre>", 
    		"try (Connection connect = DriverManager.getConnection(\"jdbc:derby:MyDbTest;create=true\")) {",
    		"    createDBCode.getEngine()",
    		"        .loadContent(createCode+\"Database created!\");",
    		"} catch (SQLException e) {",
    		"    createDBCode.getEngine()",
    		"        .loadContent(createCode+\"Unable to create database : \"+e.getMessage());",
    		"}");
    private final static String tableCode = String.format("<body style='font-size: 28px; font-family: menlo,Verdana,sans-serif'><pre>%s</pre>",
    		"try (Connection connect = DriverManager.getConnection(\"jdbc:derby:MyDbTest\")) {\n" + 
    		"    String sql = \"CREATE TABLE FIRSTTABLE\\n\" + \n" + 
    		"           \"    (ID INT PRIMARY KEY,\\n\" + \n" + 
    		"           \"    NAME VARCHAR(12))\";\n" + 
    		"    Statement stmnt = connect.createStatement();\n" + 
    		"    stmnt.execute(sql);\n" + 
    		"    createTableCode.getEngine()\n" + 
    		"        .loadContent(tableCode+\"&lt;p style='color:green'>Table created!&lt;/p>\");\n" + 
    		"} catch (SQLException e) {\n" + 
    		"    createTableCode.getEngine()\n" + 
    		"        .loadContent(tableCode+\"&lt;p style='color:red'>Unable to create table : \"+e.getMessage()+\"&lt;/p>\");\n" + 
    		"}");
    private final static String insertCode = String.format("<body style='font-size: 28px; font-family: menlo,Verdana,sans-serif'><pre>%s</pre>", 
    		"try (Connection connect = DriverManager.getConnection(\"jdbc:derby:MyDbTest\")) {\n" + 
    		"    String sql = \"INSERT INTO FIRSTTABLE VALUES (10,'TEN'),(20,'TWENTY'),(30,'THIRTY')\";\n" + 
    		"    Statement stmnt = connect.createStatement();\n" + 
    		"    int rows = stmnt.executeUpdate(sql);\n" + 
    		"    insertDataCode.getEngine().loadContent(\n" + 
    		"        String.format(\"%s%d rader satt inn i tabellen\",  insertCode, rows));\n" + 
    		"} catch (SQLException e) {\n" + 
    		"    insertDataCode.getEngine()\n" + 
    		"        .loadContent(insertCode+\"&lt;p style='color:red'>Unable to insert data : \"+e.getMessage()+\"&lt;/p>\"););\n" + 
    		"}\n" + 
    		"");
    private final static String query = String.format("<body style='font-size: 28px; font-family: menlo,Verdana,sans-serif'><pre>%s</pre>",
    		"try (Connection connect = DriverManager.getConnection(\"jdbc:derby:MyDbTest\")) {\n" + 
    		"    String sql = \"SELECT * FROM FIRSTTABLE\";\n" + 
    		"    Statement stmnt = connect.createStatement();\n" + 
    		"    ResultSet res = stmnt.executeQuery(sql);\n" + 
    		"    ResultSetMetaData meta = res.getMetaData();\n" + 
    		"    String data = \"&lt;body style='font-size: 28px; font-family: menlo,Verdana,sans-serif'>\"+\n" + 
    		"        \"&lt;table border='1'>&lt;tr>\";\n" + 
    		"    for (int i=0; i&lt;meta.getColumnCount(); i++) {\n" + 
    		"        data += \"&lt;td>\"+meta.getColumnName(i+1)+\"&lt;/td>\";\n" + 
    		"    }\n" + 
    		"    data += \"&lt;/tr>\";\n" + 
    		"    while (res.next()) {\n" + 
    		"        data += \"&lt;tr>\";\n" + 
    		"        for (int i=0; i&lt;meta.getColumnCount(); i++) {\n" + 
    		"            data += \"&lt;td>\"+res.getString(i+1)+\"&lt;/td>\";\n" + 
    		"        }\n" + 
    		"        data += \"&lt;/tr>\";\n" + 
    		"    }\n" + 
    		"    data += \"&lt;/table>\";\n" + 
    		"    queryResult.getEngine().loadContent(data);\n" + 
    		"} catch (SQLException e) {\n" + 
    		"    queryResult.getEngine().loadContent(e.getMessage());\n" + 
    		"}");
    
    @FXML 
    void initialize() {
        createDBCode.getEngine().loadContent(createCode);
        createTableCode.getEngine().loadContent(tableCode);
        insertDataCode.getEngine().loadContent(insertCode);
        queryCode.getEngine().loadContent(query);
    }

    @FXML
    void createDB(ActionEvent event) {
    	try (Connection connect = DriverManager.getConnection("jdbc:derby:MyDbTest;create=true")) {
    		createDBCode.getEngine()
    			.loadContent(createCode+"<p style='color:green'>Database created!</p>");
    	} catch (SQLException e) {
    		createDBCode.getEngine()
            	.loadContent(createCode+"<p style='color:red'>Unable to create database : "+e.getMessage()+"</p>");
		}
    }

    @FXML 
    void createTable(ActionEvent even) {
    	try (Connection connect = DriverManager.getConnection("jdbc:derby:MyDbTest")) {
    		String sql = "CREATE TABLE FIRSTTABLE\n" + 
    				"    (ID INT PRIMARY KEY,\n" + 
    				"    NAME VARCHAR(12))";
    		Statement stmnt = connect.createStatement();
    		stmnt.execute(sql);
    		createTableCode.getEngine()
            	.loadContent(tableCode+"<p style='color:green'>Table created!</p>");
    	} catch (SQLException e) {
    		createTableCode.getEngine()
            	.loadContent(tableCode+"<p style='color:red'>Unable to create table : "+e.getMessage()+"</p>");
		}
    }
    
    @FXML
    void insertData(ActionEvent event) {
    	try (Connection connect = DriverManager.getConnection("jdbc:derby:MyDbTest")) {
    		String sql = "INSERT INTO FIRSTTABLE VALUES (10,'TEN'),(20,'TWENTY'),(30,'THIRTY')";
    		Statement stmnt = connect.createStatement();
    		int rows = stmnt.executeUpdate(sql);
    		insertDataCode.getEngine().loadContent(
    				String.format("%s%d rader satt inn i tabellen",  insertCode, rows));
    	} catch (SQLException e) {
    		insertDataCode.getEngine()
    			.loadContent(insertCode+"<p style='color:red'>Unable to insert data : "+e.getMessage()+"</p>");
		}
    }

    @FXML
    void runQuery(ActionEvent event) {
    	try (Connection connect = DriverManager.getConnection("jdbc:derby:MyDbTest")) {
    		String sql = "SELECT * FROM FIRSTTABLE";
    		Statement stmnt = connect.createStatement();
    		ResultSet res = stmnt.executeQuery(sql);
    		ResultSetMetaData meta = res.getMetaData();
    		String data = "<body style='font-size: 28px; font-family: menlo,Verdana,sans-serif'>"+
    				"<table border='1'><tr>";
    		for (int i=0; i<meta.getColumnCount(); i++) {
    			data += "<td>"+meta.getColumnName(i+1)+"</td>";
    		}
    		data += "</tr>";
    		while (res.next()) {
    			data += "<tr>";
    			for (int i=0; i<meta.getColumnCount(); i++) {
    				data += "<td>"+res.getString(i+1)+"</td>";
    			}
    			data += "</tr>";
    		}
    		data += "</table>";
    		queryResult.getEngine().loadContent(data);
    	} catch (SQLException e) {
    		queryResult.getEngine().loadContent(e.getMessage());
		}
    }
}
