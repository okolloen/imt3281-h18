package no.ntnu.imt3281.buffers;

import java.util.concurrent.ArrayBlockingQueue;

public class BlockingBuffer<T> implements Buffer<T> {
	private final ArrayBlockingQueue<T> buffer;
	
	public BlockingBuffer(int size) {
		buffer = new ArrayBlockingQueue<>(size);
	}
	
	@Override
	public void put(T value) throws InterruptedException {
		buffer.put(value);
		System.out.printf("Lagt i bufferet\t\t%4s%n", value);
	}

	@Override
	public T get() throws InterruptedException {
		T value = buffer.take();
		System.out.printf("Hentet fra bufferet\t%4s%n", value!=null?value:"");
		return value;
	}
}
