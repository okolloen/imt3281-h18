package no.ntnu.imt3281.chat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * Controller for Client.fxml, there will be one controller
 * object for each client (total of 6) in the program.
 * 
 * @author oeivindk
 *
 */
public class Client {
    @FXML private ListView<String> users;
    @FXML private TextArea chat;
    @FXML private TextField username;
    @FXML private Button connect;
    @FXML private TextField text;
    @FXML private Button send;
    
    private final ObservableList<String> usernames = FXCollections.observableArrayList();
    private boolean connected = false;
    private Connection connection;
    ExecutorService executor = Executors.newFixedThreadPool(1);

    /**
     * Connects the list of user names to the listview.
     */
    @FXML
    public void initialize() {
    	users.setItems(usernames);
    }
    
    /**
     * Called from the connect/disconnect button.
     * If connected==true then disconnects and clears
     * the list of users. Sends message to server notifying it
     * of this client leaving then closes the connection.
     * Updates the connect/disconnect button.
     * If connected==false then connects to the server.
     * 
     * @param event needed as an implementation side effect, not used.
     */
    @FXML
    void connect(ActionEvent event) {
    	if (connected) {				// Currently connected, disconnect from server
			connected = false;
			connection.close();			// This will send a message to the server notifying the server about this client leaving
    		username.setDisable(false);
    		connect.setText("Connect");
    		text.setDisable(true);
    		send.setDisable(true);
    		usernames.clear();			// Empties the list of user
    	} else {
			try {
				connection = new Connection();			// Connect to the server
				connected = true;
				connection.send(username.getText()); 	// Send username
				executor.execute(()->listen());			// Starts a new thread, listening to messages from the server
	    		username.setDisable(true);
	    		connect.setText("Disconnect");
	    		text.setDisable(false);
	    		send.setDisable(false);
			} catch (UnknownHostException e) {
				// Ignored, means no connection (should have informed the user.)
			} catch (IOException e) {
				// Ignored, means no connection (should have informed the user.)
			}
    	}
    }

    // This message will run in a separate thread while the client is connected
    private void listen() {
		while (connected) {		// End when connection is closed
			try {
				if (connection.input.ready()) {		// A line can be read
					String tmp = connection.input.readLine();
					Platform.runLater(()-> {		// NOTE! IMPORTANT! DO NOT UPDATE THE GUI IN ANY OTHER WAY
						if (tmp.startsWith("JOIN:")) {
							usernames.add(tmp.substring(5));
						} else if (tmp.startsWith("DISCONNECTED:")) {
							usernames.remove(tmp.substring(13));
						} else {
							String who = tmp.substring(4, tmp.indexOf('>', 4));
							String msg = tmp.substring(tmp.indexOf('>', 4)+1);
							chat.setText(chat.getText()+who+" said: "+msg+"\n");	// this is the reason for Platform.runLater...
						}
					});
				}
				Thread.sleep(50); 	// Prevent client from using 100% CPU
			} catch (IOException e) {
				// Ignored, should have disconnected
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}

    /**
     * Connected to the send button and the textfield
     * 
     * @param event this is here as an implementation side effect, not used.
     */
	@FXML
    void sendText(ActionEvent event) {
		if (connected) {			// We only send if we are connected
			try {
				connection.send(text.getText());
				text.setText("");
			} catch (IOException e) {
				connection.close();	// If unable to send, then close the connection
			}
		}
    }
    
	/**
	 * Object of this class is used to create a connection to the server
	 * and keep information about this connection.
	 *  
	 * @author oeivindk
	 *
	 */
    class Connection {
    	private final Socket socket;
    	private final BufferedReader input;
    	private final BufferedWriter output;
    	
    	/**
    	 * Creates a new connection to the server.
    	 * 
    	 * @throws IOException
    	 */
    	public Connection() throws IOException {
    		socket = new Socket(Config.SERVERNAME, Config.SERVERPORT);
    		input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    		output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    	}
    	
    	/**
    	 * Sends a message to the server, adds a newline and flushes the bufferedWriter.

    	 * @param text the message to send (no newline)
    	 * @throws IOException thrown if error during transmission
    	 */
    	public void send(String text) throws IOException {
    		output.write(text);
    		output.newLine();
    		output.flush();
    	}
    	
    	/**
    	 * Sends a message to the server notifying it about this client
    	 * leaving and then closes the connection.
    	 */
    	public void close() {
    		try {
    			send("§§BYEBYE§§");
				output.close();
	    		input.close();
	    		socket.close();
			} catch (IOException e) {
				// Nothing to do, the connection is closed
			}
    	}
    }
}