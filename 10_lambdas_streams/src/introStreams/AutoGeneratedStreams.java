package introStreams;

import java.util.stream.IntStream;

public class AutoGeneratedStreams {
	public static void main(String[] args) {
		System.out.printf("Summen av tall fra 1-9: %d\n", 
				IntStream.range(1, 10).sum());
		System.out.printf("Summen av tall fra 1-10: %d\n", 
				IntStream.rangeClosed(1, 10).sum());
		// ==========
		System.out.printf("Summen av partall fra 2 til 20: %d\n", 
				IntStream.rangeClosed(1,  10)
				.map(x-> x*2)	// <- a lambda expression
				.sum());
	}
}