package no.ntnu.imt3281.exceptions;

public class MyException extends ArithmeticException {
	private static final long serialVersionUID = 1L;
	private int code;

	public MyException (String descr, int code) {
		super (descr);
		this.code = code;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof MyException) {
			if (((MyException)o).getCode()==code&&
					getMessage()==((MyException)o).getMessage()) {
				return true;
			}
		}
		return false;	
	}

	public int getCode() {
		return code;
	}
}
