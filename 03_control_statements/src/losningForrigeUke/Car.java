package losningForrigeUke;

public class Car {
	private String model;
	private String year;
	private double price;
	
	public Car () {
		model = "";
		year = "";
		price = 0.0;
	}

	/**
	 * @param model
	 * @param year
	 * @param price
	 */
	public Car(String model, String year, double price) {
		super();
		this.model = model;
		this.year = year;
		this.price = price;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Car [model=" + model + ", year=" + year + ", price=" + price + "]";
	}
}
