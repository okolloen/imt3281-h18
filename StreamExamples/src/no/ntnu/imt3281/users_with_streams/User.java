package no.ntnu.imt3281.users_with_streams;

/**
 * Objects of this class holds information about the id, first name,
 * last name, email, gender and ip address of a user.
 * 
 * @author oeivindk
 *
 */
class User {
	int id;
	String first;
	String last;
	String email;
	String gender;
	String ip;
	
	/**
	 * Create user object based on a line of comma separated values.
	 * 
	 * @param data a line containing id, first name, last name, email, 
	 * gender and ip address separated by commas.
	 */
	public User(String data) {
		this(data.split(","));
	}

	/**
	 * Create a new User object when the different parts 
	 * are passed as an array of strings.
	 * 
	 * @param parts an array of String containing the id, first name,
	 * last name, email, gender and ip address of the user.
	 */
	public User(String[] parts) {
		id = Integer.parseInt(parts[0]);
		first = parts[1];
		last = parts[2];
		email = parts[3];
		gender = parts[4];
		ip = parts[5];
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the first
	 */
	public String getFirst() {
		return first;
	}

	/**
	 * @return the last
	 */
	public String getLast() {
		return last;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
}