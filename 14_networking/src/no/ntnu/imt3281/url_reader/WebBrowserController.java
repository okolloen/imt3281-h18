package no.ntnu.imt3281.url_reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;

public class WebBrowserController {

	@FXML
	private TextField address;
	@FXML
	private TextArea textArea;

	@FXML
	void browse(ActionEvent event) {
		try {
			URL url = new URL(address.getText());
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
			/*
			 * URLConnection connection = url.openConnection();
			 * connection.setRequestProperty("Accept", "application/json"); BufferedReader
			 * br = new BufferedReader( new InputStreamReader(
			 * connection.getInputStream()));
			 */
			StringBuffer sb = new StringBuffer();
			String tmp;
			while ((tmp = br.readLine()) != null) {
				sb.append(tmp);
				sb.append("\n");
			}
			textArea.setText(sb.toString());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
