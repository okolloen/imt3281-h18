package no.ntnu.imt3281.io;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Users {
	@XmlElement(name="user")
	private List<User> users = new ArrayList<>();
	
	public List<User> getUsers() {
		return users;
	}
}
