package no.ntnu.imt3281.listView;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

public class ListViewControllerLive {

    @FXML
    private ListView<Browser> list;

    @FXML
    private ImageView image;
    
    private ObservableList<Browser> browsers = 
    		FXCollections.observableArrayList();
    
    @FXML
    public void initialize( ) {
    	browsers.add(new Browser ("Chrome", "images/chrome.jpg"));
    	browsers.add(new Browser("Edge", "images/edge.jpg"));
    	
    	list.setItems(browsers);
    	
    	list.getSelectionModel().selectedItemProperty().addListener((observable, old, newValue) -> {
				image.setImage(new Image(getClass().getResourceAsStream(newValue.getImg())));
			});
    	
    	list.setCellFactory(param ->  new ImageTextCell1());
    }

    class Browser {
    	String name, img;

		/**
		 * @param name
		 * @param img
		 */
		public Browser(String name, String img) {
			super();
			this.name = name;
			this.img = img;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the img
		 */
		public String getImg() {
			return img;
		}
    	
    	@Override
    	public String toString() {
    		return name;
    	}
    }
}


