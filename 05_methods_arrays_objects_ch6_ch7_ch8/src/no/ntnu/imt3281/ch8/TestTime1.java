package no.ntnu.imt3281.ch8;

import static org.junit.Assert.*;

import org.junit.Rule;

import static org.hamcrest.Matchers.is;


import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TestTime1 {
	@Rule
	final public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testConstructorUTC() {
		Time1 time = new Time1();
		assertEquals("00:00:00", time.toUniversalString());
	}
	
	@Test
	public void testToString() {
		Time1 time = new Time1();
		assertEquals("12:00:00 AM", time.toString());
	}
	
	@Test
	public void testSetTime() {
		Time1 time = new Time1();
		time.setTime(14, 34, 42);
		assertEquals("14:34:42", time.toUniversalString());
		assertEquals("2:34:42 PM", time.toString());
	}

	@Test
	public void testBadTime() {
		Time1 time = new Time1();
		
		IllegalArgumentException e = new IllegalArgumentException("hour, minute and/or second was out of range");
		//	exception.expect(is(e));	// IllegalArgumentException does not implement the equals method
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("hour, minute and/or second was out of range");
		time.setTime(99, 20, 0);
	}
}
