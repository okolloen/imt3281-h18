package no.ntnu.imt3281.polymorphism;

public class Student extends AcademicPerson {
	private String program;
	
	public Student(String givenName, String sureName, String school, String program) {
		super(givenName, sureName, school);
		this.program = program;
	}

	
	public void setProgram(String program) {
		this.program = program;
	}

	@Override
	public String toString() {
		return String.format("%s %s is studying %s at %s", getGivenName(), getSureName(), program, getSchool());
	}
}
