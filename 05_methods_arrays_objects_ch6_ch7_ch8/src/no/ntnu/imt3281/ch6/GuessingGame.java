package no.ntnu.imt3281.ch6;

import java.security.SecureRandom;
import java.util.Scanner;

public class GuessingGame {
	private enum Status {TOLOW, CORRECT, TOHIGH};
	
	public static void main(String[] args) {
		final int numberToGuess = new SecureRandom().nextInt(100)+1;
		Scanner input = new Scanner(System.in);
		System.out.println("Jeg tenker på et tall mellom 1 og 100, gjett tallet:");
		Status status;
		int number = input.nextInt();
		do {
			if (number<numberToGuess) {
				status = Status.TOLOW;
			} else if (number>numberToGuess) {
				status = Status.TOHIGH;
			} else {
				status = Status.CORRECT;
			}
			
			if (status!=Status.CORRECT) {
				System.out.println("Tallet jeg tenker på er " + 
						((status == Status.TOHIGH) ? "mindre" : "større") + ", prøv igjen:");
				number = input.nextInt();
			}
		} while (status!=Status.CORRECT);
		System.out.println("Du er god, det er riktig gjettet!");
	}
}
