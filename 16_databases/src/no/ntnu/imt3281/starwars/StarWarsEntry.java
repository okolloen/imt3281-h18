package no.ntnu.imt3281.starwars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class StarWarsEntry {
	HashMap<String, String> attributes = new HashMap<>();
	HashMap<String, List<String>> lists = new HashMap<>();

	public StarWarsEntry(String data) {
		JSONObject json = new JSONObject(data);
		json.keySet().stream().forEach(key -> {
			if (json.get(key) instanceof JSONArray) {
				List<String> list = new ArrayList<String>();
				JSONArray items = (JSONArray) json.get(key);
				items.forEach(item -> list.add((String) item));
				lists.put(key, list);
			} else {
				attributes.put(key, json.get(key).toString());
			}
		});
	}

	public String get(String key) {
		return attributes.get(key);
	}

	public List<String> getList(String key) {
		return lists.get(key);
	}
}
