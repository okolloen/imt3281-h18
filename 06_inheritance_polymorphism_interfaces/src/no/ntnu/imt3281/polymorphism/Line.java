package no.ntnu.imt3281.polymorphism;

import javafx.scene.canvas.GraphicsContext;

public class Line extends MyShape implements GraphicsShape {
	private int x1, y1, x2, y2;

	private Line(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	public static Line createLine(int x1, int y1, int x2, int y2) {
		return new Line(x1, y1, x2, y2);
	}
	
	@Override
	public void draw(GraphicsContext gc) {
		gc.strokeLine(x1, y1, x2, y2);
	}
}
