package no.ntnu.imt3281.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.bind.JAXB;

public class CreateXMLFile {
	public static void main(String[] args) {
		Users users = new Users();
		try(BufferedWriter bw = new BufferedWriter (new FileWriter("users.xml")); BufferedReader br = new BufferedReader(new FileReader("MOCK_DATA.csv"))) {
			String line;
			br.readLine(); 			// Drop first line (format explanation
			while (br.ready()) {
				line = br.readLine();
				String data[] = line.split(",");
				User u = new User(Integer.parseInt(data[0]), data[1], data[2], data[5]);
				users.getUsers().add(u);
			}
			JAXB.marshal(users, bw);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}		
	}
}
