package no.ntnu.imt3281.ch8;

public class BookTest {
	public static void main(String[] args) {
		System.out.println("All Books");
		for (Book book: Book.values()) {
			System.out.printf("%-10s%-45s%s\n", 
					book, book.getTitle(), book.getCopyrightYear());
		}
		System.out.println("\nJava How to Program book:");
		System.out.printf("%-10s%-45s%s\n", 
				Book.JHTP, Book.JHTP.getTitle(), Book.JHTP.getCopyrightYear());
	}
}
