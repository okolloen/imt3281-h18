package no.ntnu.imt3281.inheritance;

import java.lang.reflect.Method;

public class C extends A {
	public C() {
		super();
		System.out.println("Constructor of C was called");
	}
	
	@Override
	public String toString() {
		return String.format("I'm %s and my parent is : %s", getClass().getName(), super.toString());
	}
	
	public String[] getMethodNames() {
		Method m[] = getClass().getMethods();
		String names[] = new String[m.length];
		for (int i=0; i<m.length; i++) {
			names[i] = m[i].toString();
		}
		return names;
	}
}
