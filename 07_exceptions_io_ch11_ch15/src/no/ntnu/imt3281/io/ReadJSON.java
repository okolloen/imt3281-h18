package no.ntnu.imt3281.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;

public class ReadJSON {
	public static void main(String[] args) {
		try (BufferedReader br = new BufferedReader (new FileReader("MOCK_DATA.json"))) {
			StringBuffer sb = new StringBuffer();
			String line;
			while ((line = br.readLine())!=null) {
				sb.append(line);
			}
			JSONArray json = new JSONArray(sb.toString());	// Top level type is an array
			for (Object obj : json) {			// json is an array
				JSONObject o = (JSONObject)obj;	// Each element is an object (JSONObject)
				User user = new User(o.getInt("id"), o.getString("first_name"), 
						o.getString("last_name"), o.getString("ip_address"));
				System.out.printf("%-5s%-20s%-20s%s\n", user.getId(), user.getFirstName(),
						user.getLastName(), user.getIp());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
