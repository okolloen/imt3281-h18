package no.ntnu.imt3281.fileChooser;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

public class FileChooserController {

    @FXML
    private TextArea textarea;

    @FXML
    private BorderPane borderPane;
    
    @FXML
    void selectDirectory(ActionEvent event) {
    	DirectoryChooser directoryChooser = new DirectoryChooser();
    	directoryChooser.setTitle("Velg katalog");
    	
    	directoryChooser.setInitialDirectory(new File("."));
    	File f = directoryChooser.showDialog(borderPane.getScene().getWindow());
    	
    	if (f!=null) {
    		analyzePath(f.toPath());
    	} else {
    		textarea.setText("Velg en fil eller katalog");
    	}
    }

    @FXML
    void selectFile(ActionEvent event) {
    	FileChooser fileChooser = new FileChooser();
    	fileChooser.setTitle("Velg fil");
    	
    	fileChooser.setInitialDirectory(new File("."));
    	File f = fileChooser.showOpenDialog(borderPane.getScene().getWindow());
    	
    	if (f!=null) {
    		analyzePath(f.toPath());
    	} else {
    		textarea.setText("Velg en fil eller katalog");
    	}
    }

	private void analyzePath(Path path) {
		try {
			if (Files.exists(path)) {
				StringBuilder sb = new StringBuilder();
				sb.append(String.format("%s:\n", path.getFileName()));
				sb.append(String.format("%s a directory\n", Files.isDirectory(path)?"Is":"Is not"));
				sb.append(String.format("Last modified: %s\n", Files.getLastModifiedTime(path)));
				sb.append(String.format("path: %s\n", path));
				
				if (Files.isDirectory(path)) {
					sb.append("Innholdet i katalogen\n");
					DirectoryStream<Path> directoryStream;
					directoryStream = Files.newDirectoryStream(path);
					
					for (Path p : directoryStream) {
						sb.append(String.format("%s\n", p));
					}
				}
				textarea.setText(sb.toString());
			} else {
				textarea.setText(String.format("'%s' finnes ikke", path));
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	} 
}