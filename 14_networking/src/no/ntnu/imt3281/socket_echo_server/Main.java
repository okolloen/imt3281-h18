package no.ntnu.imt3281.socket_echo_server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	private static SocketEchoServerController controller;
	
	@Override
	public void start(Stage primaryStage) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("SocketEchoServer.fxml"));
		Parent root = loader.load();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("JavaFX Socket Echo Server");
		primaryStage.show();
		controller = loader.getController();	// Trenger tilgang til controller objektet fra nettverksdelen
	}

	public static void main(String[] args) {
		new Thread(()->runServer()).start();	// Kjører i egen tråd, mer om dette i neste uke
		launch(args);
		System.exit(0);	// End application when GUI closes, if not the server thread keeps running.
	}

	private static void runServer() {
		try (ServerSocket server = new ServerSocket (9010)) {
			boolean stopping = false;
			while (!stopping) {
				Socket s = server.accept();
				BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
				try (br;bw) {
					String remote = s.getInetAddress().getHostName();
					int remotePort = s.getPort();
					controller.appendToTextArea(String.format("Connected from %s on port %d", remote, remotePort));
					bw.write(String.format("Hello %s on port %d, please say something!%n", remote, remotePort));
					bw.flush();			// Remember to flush!
					String fromRemote = br.readLine();	// This is a blocking method, will not return until a line is available.
					controller.appendToTextArea(String.format("Remote said: %s",  fromRemote));
					if (fromRemote.equals("STOP")) {
						stopping = true;
						controller.appendToTextArea("Server is stopping");
					}
					bw.write(String.format("'%s' right back at you.%n", fromRemote));
					bw.flush();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
				s.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}